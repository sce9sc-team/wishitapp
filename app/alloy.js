// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

var Module = require('net.iamyellow.tiws');
Alloy.Globals.Map = require('ti.map');

//Alloy.Globals.hosturl = "http://www.dmorph.uni.me:3000";
Alloy.Globals.hosturl = "http://10.1.8.77:3000";
//Alloy.Globals.hosturl = "http://192.168.1.15:3000";
//Alloy.Globals.hosturl = "http://localhost:3000";
//Alloy.Globals.wishes = Alloy.createCollection('wish'); // This is an old aproach using the ALloy Backbone models

//init script 

var init = require('js/init');
init.startGeolocation();

/*---------------------------------------Backbone Models--------------------------------------------------------------
 * 
 */
var mymodels = require('js/wishitModels');

var friendsList = new mymodels.friendsCollection();

friendsList.on('add', function(e){
    // custom function to update the content on the view
    Ti.API.info('adding stuff');
});

friendsList.on('sync', function(e){
    // custom function to update the content on the view
    Ti.API.info('adding sync');
});

/*
friendsList.on('change', function(e){
    // custom function to update the content on the view
    Ti.API.info('adding change');
});*/

friendsList.on('remove', function(model){
    // custom function to update the content on the view
    Ti.API.info('remove' + JSON.stringify(model) );
    model.destroy();
});

Alloy.Globals.friendsList = friendsList;
Alloy.Collections.friendsList = friendsList;

// =============================Friends Places Models =============================

var friendsPlacesList = new mymodels.friendsPlacesCollection();
friendsPlacesList.on('remove', function(model){
    // custom function to update the content on the view
    Ti.API.info('remove' + JSON.stringify(model) );
    model.destroy({id:"sds"});
});

friendsPlacesList.on('reset', function(model){
    // custom function to update the content on the view
    Ti.API.info('reset' + JSON.stringify(model) );
   
});

friendsPlacesList.on('change', function(model){
    // custom function to update the content on the view
    Ti.API.info('change' + JSON.stringify(model) );
   
});

friendsPlacesList.on('add', function(model){
    // custom function to update the content on the view
    Ti.API.info('add' + JSON.stringify(model) );
   
});

Alloy.Globals.friendsPlacesList = friendsPlacesList;
Alloy.Collections.friendsPlacesList = friendsPlacesList;

// =============================Places Models =============================


var placesList = new mymodels.placesCollection();
placesList.on('remove', function(model){
    // custom function to update the content on the view
    Ti.API.info('remove' + JSON.stringify(model) );
    model.destroy({id:"sds"});
});

placesList.on('reset', function(model){
    // custom function to update the content on the view
    Ti.API.info('reset' + JSON.stringify(model) );
   
});

placesList.on('change', function(model){
    // custom function to update the content on the view
    Ti.API.info('change' + JSON.stringify(model) );
   
});

placesList.on('add', function(model){
    // custom function to update the content on the view
    Ti.API.info('add' + JSON.stringify(model) );
   
});

Alloy.Globals.placesList = placesList;
Alloy.Collections.placesList = placesList;


// =============================Places Models =============================

/*
var mapPlacesList = new mymodels.mapPlacesCollection();
mapPlacesList.on('remove', function(model){
    // custom function to update the content on the view
    Ti.API.info('remove' + JSON.stringify(model) );
    model.destroy({id:"sds"});
});
Alloy.Globals.mapPlacesList = mapPlacesList;
Alloy.Collections.mapPlacesList = mapPlacesList;

*/

// =============================Requests Model ====================



var requestsList = new mymodels.requestsCollection();


requestsList.on('remove', function(model){
    // custom function to update the content on the view
    Ti.API.info('remove' + JSON.stringify(model) );
   //model.destroy({id:"sds"});
});

requestsList.on('add', function(model){
    // custom function to update the content on the view
   Ti.API.info('request List add-------------');
    Ti.API.info(model);
   //model.destroy({id:"sds"});
});

requestsList.on('change', function(model){
    // custom function to update the content on the view
     Ti.API.info('request List change----------');
    Ti.API.info(model);
   //model.destroy({id:"sds"});
});

requestsList.on('reset', function(collection){
	//fired when fetched
    // custom function to update the content on the view
     Ti.API.info('request List reset----------');
    /*Ti.API.info(JSON.stringify(collection));
    for(var i=0; i<collection.length;i++)
    {
    	Ti.API.info(JSON.stringify(collection.models[i].attributes));
    	Alloy.Globals
    }*/
   //var requestsController = Alloy.createController('requests');
   //requestsController.createTableRows();
});

Alloy.Globals.requestsList = requestsList;
Alloy.Collections.requestsList = requestsList;

/*----------------------------------------------------------------------------------------
 			Models end
 * --------------------------------------------------------------------------------------
 * */


/*------------------------------event for network change----------------------------------------------------------*/
Ti.Network.addEventListener("change",function(){
	Ti.API.info('networkType: '+Ti.Network.networkType);
	Ti.API.info('NETWORK_WIFI: '+Ti.Network.NETWORK_WIFI);
	Ti.API.info('NETWORK_UNKNOWN: '+Ti.Network.NETWORK_UNKNOWN);
	Ti.API.info('NETWORK_MOBILE: '+Ti.Network.NETWORK_MOBILE);
	Ti.API.info('NETWORK_LAN: '+Ti.Network.NETWORK_LAN);
	Ti.API.info('NETWORK_NONE: '+Ti.Network.NETWORK_NONE);
	
});
/*------------------------------event for network change end----------------------------------------------------------*/


/*-----------------------------------register Background service -----------------------------------------------------------------------------*/
var service = Ti.App.iOS.registerBackgroundService({url:'bg_service.js'});
/*-----------------------------------register Background service  END-----------------------------------------------------------------------------*/

/*------------------------------events fired when app is on paused and resumed----------------------------------------------------------
 
 * 			When home btn is pressed or power button is pressed 
 * 
 * */
Ti.App.addEventListener('pause',function(e){
	Ti.API.info('the app just pause' + e);
	//Ti.API.info( Alloy.Globals.sio.iwthis_conn_io.socket.connected);
});

Ti.App.addEventListener('paused',function(e){
	Ti.API.info('the app just paused' + e);
	//Ti.API.info( Alloy.Globals.sio.iwthis_conn_io.socket.connected);
	
});

Ti.App.addEventListener('resume',function(e){
	Ti.API.info('the app just resume' + e);
	//Ti.API.info( Alloy.Globals.sio.iwthis_conn_io.socket.connected);
	
});

Ti.App.addEventListener('resumed',function(e){
	Ti.API.info('the app just resumed' + e);
	//Ti.API.info( Alloy.Globals.sio.iwthis_conn_io.socket.connected);
	
	
});
/*------------------------------events fired when app is on paused and resumed END----------------------------------------------------------
 */



/*------------------------------Web Sockets begin----------------------------------------------------------
 */
var websocketMethods = require('js/socketMethods'); // websocket methods in the file 
Alloy.Globals.WebSocketConnect;
Alloy.Globals.webSocketConnected = false;
Alloy.Globals.startSockets = websocketMethods.websocketStart;

/*------------------------------Web Sockets END----------------------------------------------------------
 */


Alloy.Globals.doPost = function(data,url,cookie,loadCallBack,errCallBack){//TODO: need to add also on progress
	Ti.API.info('checkIfSessionExists start');
	
	var xhr = Titanium.Network.createHTTPClient();
	//need to clear anyCookie saved
	xhr.clearCookies(Alloy.Globals.hosturl);
	xhr.onload = loadCallBack;
	xhr.onerror = errCallBack;
	xhr.open("POST",url);
	if(cookie){xhr.setRequestHeader("Cookie",cookie);}
	xhr.send(data);
};
