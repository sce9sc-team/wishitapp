


function authOnLoad(e){
	Ti.API.info('in utf-8 onload for POST');
		var rawcookie = this.getResponseHeader('Set-Cookie');
		//Ti.API.info(rawcookie);
		//Ti.API.info(this.responseText);
		var data = JSON.parse(this.responseData);
		if(data.res){
				//Need to Save the user()
				var user = {
					'cookie':rawcookie.split(" "),
					'authenticated':true,
					'user_id':data.res.user_id,
					'username':data.res.username,
					'password':data.res.password
				};
				Ti.App.Properties.setObject('user',user);
				Ti.API.info('user saved and authenticated');
				//Close the register Window
			    	/*$.registerWin.close({modal:true,
					animated:true,
					modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_FLIP_HORIZONTAL,
					modalStyle:Ti.UI.iPhone.MODAL_PRESENTATION_FULLSCREEN});*/
					
				$.trigger("openApp",e);	
				//Start WebSockets
				Alloy.Globals.startSockets(user);
		}else if (data.error){
				// need to open the the register View ot the register Window 
				Ti.API.log(data.error);
				
		}
}

function authOnError(e){
	Ti.API.info(e);
}


function registerUser(e){
	
	var data = {username:$.r_username.getValue(),
				password:$.r_password.getValue(),
				email:$.r_email.getValue()};
	Alloy.Globals.doPost(data,Alloy.Globals.hosturl+"/register",null,authOnLoad,authOnError);
}


function signinUser(e){
	
	var data = {username:$.s_username.getValue(),
				password:$.s_password.getValue()};
   	Alloy.Globals.doPost(data,Alloy.Globals.hosturl+"/login",null,authOnLoad,authOnError);
	
}

function openSignIn(e){
	//$.signinView.show();
	//$.registerView.hide();
}
function openRegister(e){
	//$.signinView.hide();
	//$.registerView.show();
}
