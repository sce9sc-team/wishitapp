Ti.App.addEventListener("sliderToggled", function(e) {
	Ti.API.info(e);
	if (e.hasSlided) {
		$.wishMapView.touchEnabled = false;
	}
	else {
		$.wishMapView.touchEnabled = true;
	}
	
});

/*----------------------------------------
 
 * 		Functions for Map View 
 * 
 * */
$.mapSearch.addEventListener("focus",function(e){
	$.mapSearchTable.setHeight(150);
	$.mapSearchTable.setVisible(true);
	$.mapSearch.setShowCancel(true, { animated: true });
});

$.mapSearch.addEventListener("blur",function(e){
	$.mapSearchTable.setHeight(0);
	$.mapSearchTable.setVisible(false);
	$.mapSearch.setShowCancel(false, { animated: true });
});

$.mapSearch.addEventListener("cancel",function(e){
	$.mapSearch.blur();
});


$.mapSearch.addEventListener("return",function(e){
	
});



/*----------------------------------------
 
 * 		Functions for Add a Wish View 
 * 
 * */


// onClick for the save btn
var saveEverything= function(){
	alert("Saving Everything");
		var views = $.wishMapView.views;
		Ti.API.info(views);
		alert(views);
};

// function for going to the Add a Wish View
function scroll(e){
	var views = $.wishMapView.views;
	$.wishMapView.scrollToView(1);
	//Call back that will be triggered when press Save
	Alloy.Globals.clbBtn = saveEverything;
	Ti.App.fireEvent("changeNav", {
		text : "Add a Wish",
		button:{name:"Save"}
	});
}


$.wishMapView.addEventListener("scrollend",function(e){
	//alert("on scrollend");
	//alert(e);
	//We can remove the view when currentPage = 0 
	
});

function fireUpTheCamera(){
	alert(1);
}

function openImageCapture(){
	alert(1);
};

function addDescription(){
	var descView = Alloy.createController('wishViewMore/descriptionView').getView();
	$.addWishTable.appendRow(descView);
};

