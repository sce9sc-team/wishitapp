Alloy.Globals.authorizeUser = false;
Alloy.Globals.trytoLoginCall;
Alloy.Globals.trytoLoginInterval = false;


function newUser(){
	//Open the Register View
	var loginView = registerController.getView('logginView');
	var signInView = registerController.getView('signinView');
	loginView.hide();
	signInView.show();
}


function onloadCallBck(e){
	Ti.API.info('checkIfSessionExists res');
	var rawcookie = this.getResponseHeader('Set-Cookie');
	//Ti.API.info(rawcookie);
	//Ti.API.info(this.responseText);
	var data = JSON.parse(this.responseData);
	if(data.res){
			//Need to Save the user()
			var user = Ti.App.Properties.getObject('user');
			if(rawcookie){
				user.cookie = rawcookie.split(" ");
				Ti.App.Properties.setObject('user',user);
				Ti.API.info('user updated');
			}
	    	Alloy.Globals.authorizeUser = true;
	    	Alloy.Globals.trytoLoginInterval = false;
			//Start WebSockets
			Alloy.Globals.startSockets(user);
	}else if (data.error){
			// need to open the the register View ot the register Window 
			Ti.API.log(data.error);
			
	}
}







function onErrorCallBck(e){
	Ti.API.info(e);
	if(!Alloy.Globals.trytoLoginInterval)
	{	
		Alloy.Globals.trytoLoginInterval = true;
		Alloy.Globals.trytoLoginCall=setInterval(function(){
    		if(!Alloy.Globals.authorizeUser){
    			trytologin();	
    		}else
    		{
    			clearInterval(Alloy.Globals.trytoLoginCall);
    			Alloy.Globals.trytoLoginInterval = false;
    		}
    	},5000);
    }
}


function authorizeUser(user)
{
	//close register Window
			/*registerWin.close({modal:true,
				animated:true,
				modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_FLIP_HORIZONTAL,
				modalStyle:Ti.UI.iPhone.MODAL_PRESENTATION_FULLSCREEN});*/
		
	$.winScroll.setCurrentPage(1);
	//Need to add a case where we have a correct session id but the server has deleted it 
	var data = {username:user.username,
				password:user.password};
	//checkIfSessionExists(data,Alloy.Globals.hosturl+"/login",user.cookie[0]);
	Alloy.Globals.doPost(data,Alloy.Globals.hosturl+"/login",user.cookie[0],onloadCallBck,onErrorCallBck);
	
}

function trytologin()
{
	//uncomment the below if you want to delete the user
	//Ti.App.Properties.removeProperty('user');
	//Check if first time user
	if(!Ti.App.Properties.hasProperty('user')){
		//we need to sign him up or create a new user
		Ti.API.info("user does not exist");
		//newUser();
	}else{
		//User exists and we need to check if the user has been previously authenticated
		Ti.API.info("Checking if user is authorized");
		var user = Ti.App.Properties.getObject('user');
		Ti.API.info(user);
		if(user.authenticated)
		{
			authorizeUser(user);
		}
	}
	

}

var registered=false;

if(registered)
{
	$.winScroll.setCurrentPage(1);
}else{
	$.winScroll.setCurrentPage(0);
	trytologin();
}

$.registerWin.on('openApp', function(e) {
		$.winScroll.scrollToView(1);
});










/*==============================*/


function rowSelect(e) {
	var views = $.ds.contentview.getViews();
	var currentView = views[$.ds.contentview.getCurrentPage()];
	if (currentView.id != e.row.viewsrc) {
		var num = _.map(views, function(num, key){ return num.id; }).indexOf(e.row.viewsrc);
		if(num==-1)
		{
			Ti.API.info(JSON.stringify(views));
			var newView = Alloy.createController(e.row.viewsrc).getView();
			$.ds.contentview.addView(newView);
			$.ds.contentview.setCurrentPage(views.length);
		}else
		{
			$.ds.contentview.setCurrentPage(num);
		}
		$.ds.navTitle.setText(e.row.title);
		
	}
}


// Pass data to widget leftTableView and rightTableView
function setupUI(){
	var leftData = [];
	var rightData = [];
	
	var leftMenuData=[{name:"Wish Map",viewsrc:"wishMapView",image:null,props:null},
					  {name:"My Wishes",viewsrc:"myWishesView",image:null,props:null},
					  {name:"Friends",viewsrc:"myFriendsView",image:null,props:null},
					  {name:"All wishes",viewsrc:"myWishesView",image:null,props:null},
					  {name:"Interested",viewsrc:"myWishesView",image:null,props:null},
					  {name:"",viewsrc:null,image:null,props:null},
					  {name:"Requests",viewsrc:"requestsView",image:null,props:null},
					  {name:"Notifications",viewsrc:"notificationsView",image:null,props:null},
					  {name:"Settings",viewsrc:"notificationsView",image:null,props:null}];
					  
   var rightMenuData=[{name:"Wish Map",viewsrc:"wishMapView",image:null,props:null},
					  {name:"My Wishes",viewsrc:"myWishesView",image:null,props:null},
					  {name:"Friends",viewsrc:"myFriendsView",image:null,props:null},
					  {name:"All wishes",viewsrc:"myWishesView",image:null,props:null},
					  {name:"Interested",viewsrc:"myWishesView",image:null,props:null},
					  {name:"",viewsrc:null,image:null,props:null},
					  {name:"Requests",viewsrc:"requestsView",image:null,props:null},
					  {name:"Notifications",viewsrc:"notificationsView",image:null,props:null},
					  {name:"Settings",viewsrc:"notificationsView",image:null,props:null}];
	
	
	for(var i=0;i<leftMenuData.length;i++)
	{
		var rowl = Alloy.createController('leftMenuRow', leftMenuData[i]).getView();
		leftData.push(rowl);
	}
	
	for(var i=0;i<rightMenuData.length;i++)
	{
		var rowr = Alloy.createController('rightMenuRow', rightMenuData[i]).getView();
		rightData.push(rowr);
	}
	$.ds.leftTableView.data = leftData;
	$.ds.rightTableView.data = rightData;
	
	var firstView = Alloy.createController("wishMapView").getView();
	$.ds.contentview.addView(firstView);
	$.ds.contentview.setCurrentPage(0);
	$.ds.navTitle.setText("Wish-it");
}

setupUI();

// Swap views on menu item click
$.ds.leftTableView.addEventListener('click', function selectRow(e) {
	if(e.row.viewsrc!=""){
		rowSelect(e);
		$.ds.toggleLeftSlider();
	}
});
$.ds.rightTableView.addEventListener('click', function selectRow(e) {
	rowSelect(e);
	$.ds.toggleRightSlider();
});

// Set row title highlight colour (left table view)
var storedRowTitle = null;
$.ds.leftTableView.addEventListener('touchstart', function(e) {
	//storedRowTitle = e.row.customTitle;
	//storedRowTitle.color = "#FFF";
});
$.ds.leftTableView.addEventListener('touchend', function(e) {
	//storedRowTitle.color = "#666";
});
$.ds.leftTableView.addEventListener('scroll', function(e) {
	//if (storedRowTitle != null)
	//	storedRowTitle.color = "#666";
});

// Set row title highlight colour (right table view)
var storedRowTitle = null;
$.ds.rightTableView.addEventListener('touchstart', function(e) {
	//storedRowTitle = e.row.customTitle;
	//storedRowTitle.color = "#FFF";
});
$.ds.rightTableView.addEventListener('touchend', function(e) {
	//storedRowTitle.color = "#666";
});
$.ds.rightTableView.addEventListener('scroll', function(e) {
	//if (storedRowTitle != null)
	//	storedRowTitle.color = "#666";
});

Ti.App.addEventListener("sliderToggled", function(e) {
	if (e.direction == "right") {
		$.ds.leftMenu.zIndex = 2;
		$.ds.rightMenu.zIndex = 1;
	} else if (e.direction == "left") {
		$.ds.leftMenu.zIndex = 1;
		$.ds.rightMenu.zIndex = 2;
	}
});





if (Ti.Platform.osname === 'iphone')
	$.win.open();
else
	$.win.open();
