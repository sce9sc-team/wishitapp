/*-----------------------------------------------------------------------------------------
 									Backbone sync overide
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * ----------------------------------------------------------------------------------------
 * */


Backbone.sync = function(method, model, options) 
{
	  Ti.API.info('starting sync');
	  options || (options = '');
	  
	  Ti.API.info(JSON.stringify(options));
	  Ti.API.info(JSON.stringify(model));
	  Ti.API.info(model.url);
	  Ti.API.info(method);
	  Ti.API.info(model.id);
	  var sockMethod = method+model.url;
	  switch (method) {
	    case 'create':
	    	Ti.API.info("Backbone create");
	    break;
	
	    case 'update':
	    	Ti.API.info("Backbone delete");
	    	if(Alloy.Globals.webSocketConnected){
			Alloy.Globals.sio.iwthis_conn_io.emit(sockMethod,model,
				function(res)
			   	{
			   		model[sockMethod](res,model,options); //fires the function in the model
			   	});
			}
	    break;
	
	    case 'delete':
	    	Ti.API.info("Backbone delete");
	    	if(Alloy.Globals.webSocketConnected){
			Alloy.Globals.sio.iwthis_conn_io.emit(sockMethod,{id:model.id},
				function(res)
			   	{
			   		model[sockMethod](res,model,options); //fires the function in the model
			   	});
			}
	    break;
	
	    case 'read':
	    	Ti.API.info("Backbone read");
	    	if(Alloy.Globals.webSocketConnected){
			Alloy.Globals.sio.iwthis_conn_io.emit(sockMethod,options,
				function(res)
			   	{
			   		model[sockMethod](res,model,options); //fires the function in the model
			   	});
			}
	    break;
	    }

};


/*----------------------------------------------------------------------------------------
 										Model for Friends 
 * 
 * 
 * 
 * 									
 * 
 * 
 * 
 * 
 * ----------------------------------------------------------------------------------------
 * */

var friendsModel = Backbone.Model.extend({
	url:'Friend',
	defaults: {
           'email':'',
    	   'username':'', 
    	   'userStatus':"offline"
        },
    initialize: function(){
            Ti.API.info('friendsModel init');
        }
});

exports.friendsModel = friendsModel;

var friendsCollection = Backbone.Collection.extend({
	model: friendsModel,
	url:"Friends",
	readFriends:function(res,model,options)
	{
		Ti.API.info('readFriends result');
	    if(res.error)
	    {
	    	Ti.API.info('error');
	    	options.error(model, error, options);
	    }
	    else
	    {
	    	var friendsList = [];
	    	for(var i=0;i<res.length;i++)
	    	{
	    		Ti.API.info(res[i]);
				var friend={
					id:res[i]._id,
					username:res[i]._source.username
				};
				friendsList.push(friend);
	    	}
	    	options.success(friendsList,JSON.stringify(friendsList),options);
    	}
	}
});
exports.friendsCollection = friendsCollection;

/*-----------------------------------------------------------------------------------------
 									Model for FriendsPlaces 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * ----------------------------------------------------------------------------------------
 * */

var friendsPlaceModel = Backbone.Model.extend({
	url:'FriendsPlace',
	defaults: {
			'user':'',
		    'name':'' ,
		    'address': '',
		    'coordinates': '',
		    'desc_item':'',
		    'image_url':'',
		    'created':'',
		    'distance':'',
		    'searchText':''
		    },
    initialize: function(){
            Ti.API.info('placeModel init');
    },
    deleteFriendsPlace:function(res,model,options){
    	Ti.API.info('delete PLace from server');
    	Ti.API.info(res);
        if(res.error)
        {
    		options.error(model, error, options);
		}else{
        	options.success();
    	}
	},
    updateFriendsPlace:function(res,model,options){
    	Ti.API.info('update FriendsPlace from server');
    	Ti.API.info(res);
        if(res.error)
        {
    		options.error(model, error, options);
		}else{
        	options.success();
    	}
	}
	
});
exports.friendsPlaceModel = friendsPlaceModel;






var friendsPlacesCollection = Backbone.Collection.extend({
	model: friendsPlaceModel,
	url:"FriendsPlaces",
	readFriendsPlaces:function(res,model,options)
	{
		Ti.API.info('result getFriendsPlaces');
   		Ti.API.info(res);
        if(res.error)
        {
        	Ti.API.info('error');
        	options.error(error);
        }else{
        	var list=[];
        	for(var i=0;i<res.length;i++)
        	{
        		Ti.API.info(res[i]);
        		var d = 
        			{
        				id:res[i]._id,
        				user:res[i]._source.user,
        				name:res[i]._source.name, 
            			desc_item:res[i]._source.desc_item,
            			image_url:Alloy.Globals.hosturl+res[i]._source.image_url,
            			coordinates:res[i]._source.coordinates,
            			address:res[i]._source.address,
            			created:res[i]._source.created,
            			distance:calculateDistance([Alloy.Globals.currentPos.lat,Alloy.Globals.currentPos.longi],res[i]._source.coordinates.split(',')),
            			searchText:	res[i]._source.name +res[i]._source.desc_item + res[i]._source.address
        			};
				list.push(d);
        	}
        	Ti.API.info(list);
        	Ti.API.info(options);
        	options.success(list);
        }
	}
});
exports.friendsPlacesCollection = friendsPlacesCollection;




/*-----------------------------------------------------------------------------------------
 									Model for Places 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * ----------------------------------------------------------------------------------------
 * */



var placeModel = Backbone.Model.extend({
	url:'Place',
	defaults: {
			'user':'',
		    'name':'' ,
		    'address': '',
		    'coordinates': '',
		    'desc_item':'',
		    'image_url':'',
		    'created':'',
		    'distance':'',
		    'searchText':''
		    },
    initialize: function(){
            Ti.API.info('placeModel init');
    },
    deletePlace:function(res,model,options){
    	Ti.API.info('delete PLace from server');
    	Ti.API.info(res);
        if(res.error)
        {
    		options.error(model, error, options);
		}else{
        	options.success();
    	}
	},
    updatePlace:function(res,model,options){
    	Ti.API.info('delete PLace from server');
    	Ti.API.info(res);
        if(res.error)
        {
    		options.error(model, error, options);
		}else{
        	options.success();
    	}
	}
	
});
exports.placeModel = placeModel;


function calculateDistance(pA,pB){
	
	function toRad(deg){
		return deg * Math.PI / 180;
	};
	
	function degCoo2rad(coo){
	    return [toRad(coo[0]),toRad(coo[1])];
	}
	
	var pBr=degCoo2rad(pB);
	var pAr=degCoo2rad(pA);

	dist = Math.acos( Math.sin(pBr[0]) *  Math.sin(pAr[0]) +  Math.cos(pBr[0]) * Math.cos(pAr[0]) * Math.cos(pBr[1] - pAr[1])) * 6371;
	return dist.toFixed(3) + " Km";
	
};



var placesCollection = Backbone.Collection.extend({
	model: placeModel,
	url:"Places",
	readPlaces:function(res,model,options)
	{
		Ti.API.info('result getPlaces');
   		Ti.API.info(res);
        if(res.error)
        {
        	Ti.API.info('error');
        	options.error(error);
        }else{
        	var list=[];
        	for(var i=0;i<res.length;i++)
        	{
        		Ti.API.info(res[i]);
        		var d = 
        			{
        				id:res[i]._id,
        				user:res[i]._source.user,
        				name:res[i]._source.name, 
            			desc_item:res[i]._source.desc_item,
            			image_url:Alloy.Globals.hosturl+res[i]._source.image_url,
            			coordinates:res[i]._source.coordinates,
            			address:res[i]._source.address,
            			created:res[i]._source.created,
            			distance:calculateDistance([Alloy.Globals.currentPos.lat,Alloy.Globals.currentPos.longi],res[i]._source.coordinates.split(',')),
            			searchText:	res[i]._source.name +res[i]._source.desc_item + res[i]._source.address
        			};
				list.push(d);
        	}
        	Ti.API.info(list);
        	Ti.API.info(options);
        	options.success(list);
        }
	}
});
exports.placesCollection = placesCollection;


/*-----------------------------------------------------------------------------------------
 									Model for Requests 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * ----------------------------------------------------------------------------------------
 * */



var requestModel = Backbone.Model.extend({
	url:'Request',
	defaults: {
			'user':'',
		    'ntype': '',
		    'status': '',
		    'created':''
		    },
    initialize: function(){
            Ti.API.info('requestModel init');
    },
    deleteRequest:function(res,model,options){
    	Ti.API.info('delete Request from server');
    	Ti.API.info(res);
        if(res.error)
        {
    		options.error(model, error, options);
		}else{
        	options.success();
    	}
	}
});
exports.requestModel = requestModel;

var requestsCollection = Backbone.Collection.extend({
	model: requestModel,
	url:"Requests",
	readRequests:function(res,model,options)
	{
		Ti.API.info('result getRequests');
   		Ti.API.info(res);
        if(res.error)
        {
        	Ti.API.info('error');
        	options.error(model, error, options);
        }else{
        	var list=[];
        	for(var i=0;i<res.length;i++)
        	{
        		Ti.API.info(res[i]);
        		var d = 
        			{
        				id:res[i]._id,
        				user:res[i].user,
        				ntype:res[i].ntype,
            			status:res[i].status,
            			created:res[i].created
        			};
				list.push(d);
        	}
        	options.success(list,JSON.stringify(list),options);
        }
	}
});
exports.requestsCollection = requestsCollection;



/*-----------------------------------------------------------------------------------------
 									Model for Map Places 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * ----------------------------------------------------------------------------------------
 * */


/*
var mapPlaceModel = Backbone.Model.extend({
	url:'MapPlace',
	defaults: {
			'user':'',
		    'name':'' ,
		    'address': '',
		    'coordinates': '',
		    'desc_item':'',
		    'image_url':'',
		    'created':''
		    },
    initialize: function(){
            Ti.API.info('MapPlace model init');
    },
    deleteMapPlace:function(res,model,options){
    	Ti.API.info('delete MapPlace from server');
    	Ti.API.info(res);
        if(res.error)
        {
    		options.error(model, error, options);
		}else{
        	options.success();
    	}
	}
});
exports.mapPlaceModel = mapPlaceModel;

var mapPlacesCollection = Backbone.Collection.extend({
	model: mapPlaceModel,
	url:"MapPlaces",
	readMapPlaces:function(res,model,options)
	{
		Ti.API.info('result readMapPlaces');
   		Ti.API.info(res);
        if(res.error)
        {
        	Ti.API.info('error');
        	options.error(model, error, options);
        }else{
        	var list=[];
        	for(var i=0;i<res.length;i++)
        	{
        		Ti.API.info(res[i]);
        		var d = 
        			{
        				id:res[i]._id,
        				user:res[i]._source.user,
        				name:res[i]._source.name, 
            			desc_item:res[i]._source.desc_item,
            			image_url:Alloy.Globals.hosturl+res[i]._source.image_url,
            			coordinates:res[i]._source.coordinates,
            			address:res[i]._source.address,
            			created:res[i]._source.created
        			};
				list.push(d);
        	}
        	options.success(list,JSON.stringify(list),options);
        }
	}
});
exports.mapPlacesCollection = mapPlacesCollection;

*/