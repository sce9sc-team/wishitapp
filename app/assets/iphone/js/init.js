exports.startGeolocation = function(){
	Ti.API.info('starting geolocation');
	Ti.Geolocation.preferredProvider = "gps";
	Ti.Geolocation.purpose = "Allow wish-it to use your GPS";
	if (Titanium.Geolocation.locationServicesEnabled === false)
	{
		Ti.API.info('Your device has geo turned off');
		Titanium.UI.createAlertDialog({title:'Wisht-it App', message:'Your device has geo turned off - turn it on.'}).show();
	}
	else
	{
		Ti.API.info('Your sfddsfssfs');
		var authorization = Titanium.Geolocation.locationServicesAuthorization;
			Ti.API.info('Authorization: '+authorization);
			if (authorization == Titanium.Geolocation.AUTHORIZATION_DENIED) {
				Ti.UI.createAlertDialog({
					title:'Wish-it App',
					message:'You have disallowed Wish-it App from running geolocation services.'
				}).show();
			}
			else if (authorization == Titanium.Geolocation.AUTHORIZATION_RESTRICTED) {
				Ti.UI.createAlertDialog({
					title:'Wish-it App',
					message:'Your system has disallowed Wish-it App from running geolocation services.'
				}).show();
			}
	}
	Ti.API.info('Your sfddsfssfs');
	
	//
		//  SET ACCURACY - THE FOLLOWING VALUES ARE SUPPORTED
		//
		// Titanium.Geolocation.ACCURACY_BEST
		// Titanium.Geolocation.ACCURACY_NEAREST_TEN_METERS
		// Titanium.Geolocation.ACCURACY_HUNDRED_METERS
		// Titanium.Geolocation.ACCURACY_KILOMETER
		// Titanium.Geolocation.ACCURACY_THREE_KILOMETERS
		//
		Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
	
		//
		//  SET DISTANCE FILTER.  THIS DICTATES HOW OFTEN AN EVENT FIRES BASED ON THE DISTANCE THE DEVICE MOVES
		//  THIS VALUE IS IN METERS
		//
		Titanium.Geolocation.distanceFilter = 1000;
		
	
		setUserLocationRegion();
		
			
		var locationCallback = function(e){
			if (!e.success || e.error)
			{
				
				return;
				
			}
			
			var longitude1 = e.coords.longitude;
			var latitude1 = e.coords.latitude;
			var altitude = e.coords.altitude;
			var heading = e.coords.heading;
			var accuracy = e.coords.accuracy;
			var speed = e.coords.speed;
			var timestamp = e.coords.timestamp;
			var altitudeAccuracy = e.coords.altitudeAccuracy;
	
			Alloy.Globals.currentPos ={lat:latitude1,longi:longitude1};

	
			Titanium.API.info('geo - location updated: ' + new Date(timestamp) + ' long ' + longitude1 + ' lat ' + latitude1 + ' accuracy ' + accuracy);
			
			Titanium.API.info(e);
			
			
			
			
			if(Alloy.Globals.webSocketConnected)
			{
				Ti.API.info('try to get Friends Places');
				 Alloy.Globals.sio.iwthis_conn_io.emit('getFriendsPlaces',{coordinates:latitude1+","+longitude1},
				function(res)
			   	{
			   		Ti.API.info('res--------------');
			            if(res.error)
			            {
			            	Ti.API.info('error');
			            }else{
			            	//createFriendsAnnotations(res);
			            	Ti.API.info('friends res '+res);
			            	var curNotif = Ti.App.iOS.scheduleLocalNotification({
			    			alertBody:'A friend has a wish near',
			    			date:new Date(new Date().getTime() + 1000) // 1 second after pause
			  				});
			            }	
			   	});
			   	
			 }
			
			//getFriendsPlaces(e);
			
			
		};
		Titanium.Geolocation.addEventListener('location', locationCallback);
		//Titanium.Geolocation.addEventListener('locationupdatepaused', function(e){Ti.API.info("locationupdatepaused");});
		//Titanium.Geolocation.addEventListener('locationupdateresumed', function(e){Ti.API.info("locationupdateresumed");});
	
		
};


function setUserLocationRegion(){

	Titanium.Geolocation.getCurrentPosition(function(e)
			{
				if (!e.success || e.error)
				{
					
					//currentLocation.text = 'error: ' + JSON.stringify(e.error);
						//				Ti.API.info("Code translation: "+translateErrorCode(e.code));
							//			alert('error ' + JSON.stringify(e.error));
					
					return;
				}
		
				var longitude1 = e.coords.longitude;
				var latitude1 = e.coords.latitude;
				var altitude = e.coords.altitude;
				var heading = e.coords.heading;
				var accuracy = e.coords.accuracy;
				var speed = e.coords.speed;
				var timestamp = e.coords.timestamp;
				var altitudeAccuracy = e.coords.altitudeAccuracy;
				Alloy.Globals.currentPos ={lat:latitude1,longi:longitude1};
				Ti.API.info('speed ' + speed);
				Ti.API.info('long:' + longitude1 + ' lat: ' + latitude1);
			
				Titanium.API.info('geo - current location: ' + new Date(timestamp) + ' long ' + longitude1 + ' lat ' + latitude1 + ' accuracy ' + accuracy);
				
			});
};
