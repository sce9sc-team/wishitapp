
exports.websocketStart = function(user)
{
//Starting sockets
	Ti.API.info('Starting sockets');
	var io = require('js/socket.io');
	
	var cookie =user.cookie[0].slice(0,user.cookie[0].length -1);
	Ti.API.info(cookie);
	var iwthis_conn_io = io.connect(Alloy.Globals.hosturl+'?'+cookie,{reconnect:false});
	
	
	iwthis_conn_io.on('connect', function () {
    	Ti.API.info('connected');
    	//Ti.API.info(iwthis_conn_io.toSource());
    	Alloy.Globals.webSocketConnected = true;
    	Alloy.Globals.placesList.fetch(); //TODO:Problem here . Gps has not been initialized; need to Disallow access when GPS is not enabled
    	Alloy.Globals.friendsList.fetch();
    	//Alloy.Globals.requestsList.fetch(); // we are doing it on open
    	//send if you are online or not
    	if(Alloy.Globals.webSocketConnected){
		Alloy.Globals.sio.iwthis_conn_io.emit('userStatus',{status:"online"},
			function(res)
		   	{
		   		Ti.API.info('response from userStatus-------------');
		   		Ti.API.info(res);
		   	});
		}
		
    });
    
    /*---------------------This is used to get the user status if online or not 
     					START
     * -------------------------*/
    iwthis_conn_io.on('userStatusReply',function(data){
    	var friend = Alloy.Globals.friendsList.get(data.user_id);  
    	Ti.API.info("this is user statusReply");
    	Ti.API.info(JSON.stringify(friend));
    	//TODO: problem here to many times is fired . problem when adding a user . should disable after first press
    	friend.set({userStatus:data.status});
    });
    
    
    iwthis_conn_io.on('userStatus',function(data){
    	
    	Ti.API.info('your friends are ');
    	var friend = Alloy.Globals.friendsList.get(data.user_id);  
    	friend.set({userStatus:data.status});
    	
    	//check if the status of the user is online . if he disconnected no need to send our status
    	if(data.status=="online"){
    		//we need to send a reply to our friend that we are online as well because we appear offline in his list 
	    	if(Alloy.Globals.webSocketConnected)
	    	{
				Alloy.Globals.sio.iwthis_conn_io.emit('userStatusReply',{status:"online",friend_id:data.user_id},
					function(res)
				   	{
				   		Ti.API.info('response from userStatusReply-------------');
				   		Ti.API.info(res);
				   	});
			}
		}
		
    });
    
     /*---------------------This is used to get the user status if online or not 
     							END
     * -------------------------*/
    
    iwthis_conn_io.on('error', function (e) {
    	Ti.API.info('a socket error occured' + e);
    	if(e=='A connection failure occurred')
    	{
    			// do something
    	}
    });
    
    iwthis_conn_io.on('notification',function(){
    	Ti.API.info('you have a notification');
    	alert('you have a notification');
    });
    
    // 
    iwthis_conn_io.on('request',function(data){
    	// after a request was send to the server the server broadcasts a message to the user that the request was intented
    	// here we should go and fetch the request and addit to the request model   	
    	
    	Ti.API.info('you have a request');
    	Ti.API.info(data);
    	alert('you have a request');
    });
    
    iwthis_conn_io.on('msg',function(){
    	Ti.API.info('you have a msg');
    	//alert('you have a msg');
    	var curNotif = Ti.App.iOS.scheduleLocalNotification({
    			alertBody:'test',
    			date:new Date(new Date().getTime() + 1000) // 1 second after pause
  				});
    });
    
    iwthis_conn_io.on('message',function(){
    	Ti.API.info('you have a message');
    	alert('you have a message');
    });
    
     iwthis_conn_io.on('anything',function(){
    	Ti.API.info('you have a anything');
    	alert('you have a anything');
    });
    
     iwthis_conn_io.on('connecting',function(){
    	Ti.API.info('you have a connecting');
    	
    });
    
     iwthis_conn_io.on('reconnecting',function(){
    	Ti.API.info('you are reconnecting');
    	
    });
    
    
    iwthis_conn_io.on('disconnect',function(){
    	Ti.API.info('you have disconnected');
    	Alloy.Globals.webSocketConnected = false;
    	
    	Alloy.Globals.WebSocketConnect=setInterval(function(){
    		if(!Alloy.Globals.webSocketConnected){
    		 iwthis_conn_io.socket.connect();	
    		}else
    		{
    			clearInterval(Alloy.Globals.WebSocketConnect);
    		}
    	},500);
    	
    	/*var dialog = Ti.UI.createAlertDialog({
	    cancel: 1,
	    buttonNames: ['Confirm', 'Cancel', 'Help'],
	    message: 'Would you like to delete the file?',
	    title: 'Delete'
	  });
	  dialog.addEventListener('click', function(e){
	    if (e.index === e.source.cancel){
	      Ti.API.info('The cancel button was clicked');
	    }
	  
	    iwthis_conn_io.socket.connect();	
	  
	    Ti.API.info('e.cancel: ' + e.cancel);
	    Ti.API.info('e.source.cancel: ' + e.source.cancel);
	    Ti.API.info('e.index: ' + e.index);
	  });
	  
	  dialog.show();*/
		
    	//iwthis_conn_io = io.connect(Alloy.Globals.hosturl,{query:cookie,'force new connection':true});
    	//iwthis_conn_io.connect();
    });
    
    iwthis_conn_io.on('connect_failed',function(){
    	Ti.API.info('you have connect_failed');
    	
    });
    
    iwthis_conn_io.on('reconnect_failed',function(){
    	Ti.API.info('you have reconnect_failed');
    	
    });
    
    
    Alloy.Globals.sio = {sio:io,iwthis_conn_io:iwthis_conn_io};
    Ti.API.info('sockets finished');
    
};