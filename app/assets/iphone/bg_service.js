
/*
Ti.API.info('bg-service1: service has been invoked once, and will now be stopped to release it from memory. ');
Ti.App.currentService.stop();

var listener = Ti.App.currentService.addEventListener('stop',function(){
  Ti.API.info('bg-service1: Although the service has been stopped, it is still registered and will be executed again on next pause');
  Ti.API.info('bg-service1: As all background services are automatically stopped on resume, it is not always necessary to explicitly stop a service');
});*/

//var io = require('js/socket.io');

function locateIt(){
	//Ti.API.info("locate it");
}


//trackTimerBG = setInterval(locateIt, 10*1000);

function initCurrentLocation(){
	Ti.API.info("starting background geolocation");
	Ti.Geolocation.preferredProvider = "gps";
	Ti.Geolocation.purpose = "GPS demo";
	if (Titanium.Geolocation.locationServicesEnabled === false)
	{
		Titanium.UI.createAlertDialog({title:'Wish-it', message:'Your device has geo turned off - turn it on.'}).show();
	}
	else
	{
		var authorization = Titanium.Geolocation.locationServicesAuthorization;
			Ti.API.info('Authorization: '+authorization);
			if (authorization == Titanium.Geolocation.AUTHORIZATION_DENIED) {
				Ti.UI.createAlertDialog({
					title:'Kitchen Sink',
					message:'You have disallowed Titanium from running geolocation services.'
				}).show();
			}
			else if (authorization == Titanium.Geolocation.AUTHORIZATION_RESTRICTED) {
				Ti.UI.createAlertDialog({
					title:'Kitchen Sink',
					message:'Your system has disallowed Titanium from running geolocation services.'
				}).show();
			}
	}
	
	
	//
		//  SET ACCURACY - THE FOLLOWING VALUES ARE SUPPORTED
		//
		// Titanium.Geolocation.ACCURACY_BEST
		// Titanium.Geolocation.ACCURACY_NEAREST_TEN_METERS
		// Titanium.Geolocation.ACCURACY_HUNDRED_METERS
		// Titanium.Geolocation.ACCURACY_KILOMETER
		// Titanium.Geolocation.ACCURACY_THREE_KILOMETERS
		//
		Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
	
		//
		//  SET DISTANCE FILTER.  THIS DICTATES HOW OFTEN AN EVENT FIRES BASED ON THE DISTANCE THE DEVICE MOVES
		//  THIS VALUE IS IN METERS
		//
		Titanium.Geolocation.distanceFilter = 1;
		var locationCallback = function(e){
			Ti.API.info("firing background location ");
			if (!e.success || e.error)
			{
				return;
			}
			//var map = $.map;
			//if(map.annotations.length!=0)
			//{
			//	var myan= map.annotations[0];
			//	myan.setLatitude(e.coords.latitude);
			//	myan.setLongitude(e.coords.longitude);	
			//}
			
			
			
			
			
			var longitude1 = e.coords.longitude;
			var latitude1 = e.coords.latitude;
			var altitude = e.coords.altitude;
			var heading = e.coords.heading;
			var accuracy = e.coords.accuracy;
			var speed = e.coords.speed;
			var timestamp = e.coords.timestamp;
			var altitudeAccuracy = e.coords.altitudeAccuracy;
	
			
			//use below if you want your map to focus on the your location
			//$.wishMap.setLocation({
    		//latitude:latitude1, longitude:longitude1, animate:true,
    		//});
			//Titanium.Geolocation.distanceFilter = 100; //changed after first location event
	
	
			Titanium.API.info('geo - location updated: ' + new Date(timestamp) + ' long ' + longitude1 + ' lat ' + latitude1 + ' accuracy ' + accuracy);
			
			Titanium.API.info(e);
	
		};
		Titanium.Geolocation.addEventListener('location', locationCallback);
		
}




//initCurrentLocation();

//trytologin();
Ti.API.info('bg-service1: has started');

var listener = Ti.App.currentService.addEventListener('stop',function(){
	//io.disconnect();
 	//Ti.App.currentService.stop();
 	Ti.API.info('bg-service1: Although the service has been stopped, it is still registered and will be executed again on next pause');
});

/*
var count = Ti.App.Properties.getInt('bg-svc2-count', 0);

if (count > 4){
  // reset count after 4 invocations
  count = 0;
}

count++;

Ti.App.Properties.setInt('bg-svc2-count', count);

Ti.API.info('bg-service2 has been run ' + count + ' times');

if (count > 4){
  Ti.App.currentService.unregister();
  var finalNotif = Ti.App.iOS.scheduleLocalNotification({
    alertBody:'bg-service2: As service has been invoked more than 4 times, it has been unregistered and will NOT run again. Relaunch the app to re-register it',
    date:new Date(new Date().getTime() + 1000) // 1 second after unregister
  });   
} else {
  var curNotif = Ti.App.iOS.scheduleLocalNotification({
    alertBody:'bg-service2: has been invoked ' + count + ' times. It is still registered and will run again when the app is transitioned to the background',
    date:new Date(new Date().getTime() + 1000) // 1 second after pause
  });   
}*/