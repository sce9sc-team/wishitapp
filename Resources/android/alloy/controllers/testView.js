function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "testView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.testView = Ti.UI.createView({
        id: "testView",
        backgroundColor: "green"
    });
    $.__views.testView && $.addTopLevelView($.__views.testView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.testView.addEventListener("open", function() {
        alert("view open");
    });
    $.testView.addEventListener("close", function() {
        alert("view close");
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;