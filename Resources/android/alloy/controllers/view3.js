function Controller() {
    function openView() {
        var testView = Alloy.createController("testView").getView();
        $.map.animate({
            view: testView,
            transition: Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
        }, function(e) {
            alert(e);
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "view3";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.view3 = Ti.UI.createView({
        id: "view3",
        backgroundColor: "yellow"
    });
    $.__views.view3 && $.addTopLevelView($.__views.view3);
    $.__views.map = Ti.UI.createView({
        id: "map",
        backgroundColor: "blue"
    });
    $.__views.view3.add($.__views.map);
    $.__views.__alloyId21 = Ti.UI.createButton({
        title: "Open testView",
        id: "__alloyId21"
    });
    $.__views.map.add($.__views.__alloyId21);
    openView ? $.__views.__alloyId21.addEventListener("click", openView) : __defers["$.__views.__alloyId21!click!openView"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("sliderToggled", function(e) {
        e.hasSlided;
    });
    __defers["$.__views.__alloyId21!click!openView"] && $.__views.__alloyId21.addEventListener("click", openView);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;