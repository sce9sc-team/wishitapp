function Controller() {
    function test() {
        alert(1);
        $.ds.contentview.remove(currentView);
        currentView = Alloy.createController("view1").getView();
        $.ds.contentview.add(currentView);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "view2";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.view2 = Ti.UI.createView({
        id: "view2"
    });
    $.__views.view2 && $.addTopLevelView($.__views.view2);
    var __alloyId14 = [];
    $.__views.sectionFruit = Ti.UI.createTableViewSection({
        id: "sectionFruit",
        headerTitle: "Fruit"
    });
    __alloyId14.push($.__views.sectionFruit);
    $.__views.__alloyId15 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId15"
    });
    $.__views.sectionFruit.add($.__views.__alloyId15);
    test ? $.__views.__alloyId15.addEventListener("click", test) : __defers["$.__views.__alloyId15!click!test"] = true;
    $.__views.__alloyId16 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId16"
    });
    $.__views.sectionFruit.add($.__views.__alloyId16);
    $.__views.sectionVeg = Ti.UI.createTableViewSection({
        id: "sectionVeg",
        headerTitle: "Vegetables"
    });
    __alloyId14.push($.__views.sectionVeg);
    $.__views.__alloyId17 = Ti.UI.createTableViewRow({
        title: "Carrots",
        id: "__alloyId17"
    });
    $.__views.sectionVeg.add($.__views.__alloyId17);
    $.__views.__alloyId18 = Ti.UI.createTableViewRow({
        title: "Potatoes",
        id: "__alloyId18"
    });
    $.__views.sectionVeg.add($.__views.__alloyId18);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId14.push($.__views.sectionFish);
    $.__views.__alloyId19 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId19"
    });
    $.__views.sectionFish.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId20"
    });
    $.__views.sectionFish.add($.__views.__alloyId20);
    $.__views.table = Ti.UI.createTableView({
        data: __alloyId14,
        id: "table"
    });
    $.__views.view2.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("sliderToggled", function(e) {
        Ti.API.info(e);
        $.table.touchEnabled = e.hasSlided ? false : true;
    });
    __defers["$.__views.__alloyId15!click!test"] && $.__views.__alloyId15.addEventListener("click", test);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;