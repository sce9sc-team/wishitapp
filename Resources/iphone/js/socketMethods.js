exports.websocketStart = function(user) {
    Ti.API.info("Starting sockets");
    var io = require("js/socket.io");
    var cookie = user.cookie[0].slice(0, user.cookie[0].length - 1);
    Ti.API.info(cookie);
    var iwthis_conn_io = io.connect(Alloy.Globals.hosturl + "?" + cookie, {
        reconnect: false
    });
    iwthis_conn_io.on("connect", function() {
        Ti.API.info("connected");
        Alloy.Globals.webSocketConnected = true;
        Alloy.Globals.placesList.fetch();
        Alloy.Globals.friendsList.fetch();
        Alloy.Globals.webSocketConnected && Alloy.Globals.sio.iwthis_conn_io.emit("userStatus", {
            status: "online"
        }, function(res) {
            Ti.API.info("response from userStatus-------------");
            Ti.API.info(res);
        });
    });
    iwthis_conn_io.on("userStatusReply", function(data) {
        var friend = Alloy.Globals.friendsList.get(data.user_id);
        Ti.API.info("this is user statusReply");
        Ti.API.info(JSON.stringify(friend));
        friend.set({
            userStatus: data.status
        });
    });
    iwthis_conn_io.on("userStatus", function(data) {
        Ti.API.info("your friends are ");
        var friend = Alloy.Globals.friendsList.get(data.user_id);
        friend.set({
            userStatus: data.status
        });
        "online" == data.status && Alloy.Globals.webSocketConnected && Alloy.Globals.sio.iwthis_conn_io.emit("userStatusReply", {
            status: "online",
            friend_id: data.user_id
        }, function(res) {
            Ti.API.info("response from userStatusReply-------------");
            Ti.API.info(res);
        });
    });
    iwthis_conn_io.on("error", function(e) {
        Ti.API.info("a socket error occured" + e);
        "A connection failure occurred" == e;
    });
    iwthis_conn_io.on("notification", function() {
        Ti.API.info("you have a notification");
        alert("you have a notification");
    });
    iwthis_conn_io.on("request", function(data) {
        Ti.API.info("you have a request");
        Ti.API.info(data);
        alert("you have a request");
    });
    iwthis_conn_io.on("msg", function() {
        Ti.API.info("you have a msg");
        Ti.App.iOS.scheduleLocalNotification({
            alertBody: "test",
            date: new Date(new Date().getTime() + 1e3)
        });
    });
    iwthis_conn_io.on("message", function() {
        Ti.API.info("you have a message");
        alert("you have a message");
    });
    iwthis_conn_io.on("anything", function() {
        Ti.API.info("you have a anything");
        alert("you have a anything");
    });
    iwthis_conn_io.on("connecting", function() {
        Ti.API.info("you have a connecting");
    });
    iwthis_conn_io.on("reconnecting", function() {
        Ti.API.info("you are reconnecting");
    });
    iwthis_conn_io.on("disconnect", function() {
        Ti.API.info("you have disconnected");
        Alloy.Globals.webSocketConnected = false;
        Alloy.Globals.WebSocketConnect = setInterval(function() {
            Alloy.Globals.webSocketConnected ? clearInterval(Alloy.Globals.WebSocketConnect) : iwthis_conn_io.socket.connect();
        }, 500);
    });
    iwthis_conn_io.on("connect_failed", function() {
        Ti.API.info("you have connect_failed");
    });
    iwthis_conn_io.on("reconnect_failed", function() {
        Ti.API.info("you have reconnect_failed");
    });
    Alloy.Globals.sio = {
        sio: io,
        iwthis_conn_io: iwthis_conn_io
    };
    Ti.API.info("sockets finished");
};