function setUserLocationRegion() {
    Titanium.Geolocation.getCurrentPosition(function(e) {
        if (!e.success || e.error) return;
        var longitude1 = e.coords.longitude;
        var latitude1 = e.coords.latitude;
        e.coords.altitude;
        e.coords.heading;
        var accuracy = e.coords.accuracy;
        var speed = e.coords.speed;
        var timestamp = e.coords.timestamp;
        e.coords.altitudeAccuracy;
        Alloy.Globals.currentPos = {
            lat: latitude1,
            longi: longitude1
        };
        Ti.API.info("speed " + speed);
        Ti.API.info("long:" + longitude1 + " lat: " + latitude1);
        Titanium.API.info("geo - current location: " + new Date(timestamp) + " long " + longitude1 + " lat " + latitude1 + " accuracy " + accuracy);
    });
}

exports.startGeolocation = function() {
    Ti.API.info("starting geolocation");
    Ti.Geolocation.preferredProvider = "gps";
    Ti.Geolocation.purpose = "Allow wish-it to use your GPS";
    if (false === Titanium.Geolocation.locationServicesEnabled) {
        Ti.API.info("Your device has geo turned off");
        Titanium.UI.createAlertDialog({
            title: "Wisht-it App",
            message: "Your device has geo turned off - turn it on."
        }).show();
    } else {
        Ti.API.info("Your sfddsfssfs");
        var authorization = Titanium.Geolocation.locationServicesAuthorization;
        Ti.API.info("Authorization: " + authorization);
        authorization == Titanium.Geolocation.AUTHORIZATION_DENIED ? Ti.UI.createAlertDialog({
            title: "Wish-it App",
            message: "You have disallowed Wish-it App from running geolocation services."
        }).show() : authorization == Titanium.Geolocation.AUTHORIZATION_RESTRICTED && Ti.UI.createAlertDialog({
            title: "Wish-it App",
            message: "Your system has disallowed Wish-it App from running geolocation services."
        }).show();
    }
    Ti.API.info("Your sfddsfssfs");
    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    Titanium.Geolocation.distanceFilter = 1e3;
    setUserLocationRegion();
    var locationCallback = function(e) {
        if (!e.success || e.error) return;
        var longitude1 = e.coords.longitude;
        var latitude1 = e.coords.latitude;
        e.coords.altitude;
        e.coords.heading;
        var accuracy = e.coords.accuracy;
        e.coords.speed;
        var timestamp = e.coords.timestamp;
        e.coords.altitudeAccuracy;
        Alloy.Globals.currentPos = {
            lat: latitude1,
            longi: longitude1
        };
        Titanium.API.info("geo - location updated: " + new Date(timestamp) + " long " + longitude1 + " lat " + latitude1 + " accuracy " + accuracy);
        Titanium.API.info(e);
        if (Alloy.Globals.webSocketConnected) {
            Ti.API.info("try to get Friends Places");
            Alloy.Globals.sio.iwthis_conn_io.emit("getFriendsPlaces", {
                coordinates: latitude1 + "," + longitude1
            }, function(res) {
                Ti.API.info("res--------------");
                if (res.error) Ti.API.info("error"); else {
                    Ti.API.info("friends res " + res);
                    Ti.App.iOS.scheduleLocalNotification({
                        alertBody: "A friend has a wish near",
                        date: new Date(new Date().getTime() + 1e3)
                    });
                }
            });
        }
    };
    Titanium.Geolocation.addEventListener("location", locationCallback);
};