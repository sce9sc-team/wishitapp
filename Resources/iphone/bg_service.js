function locateIt() {}

function initCurrentLocation() {
    Ti.API.info("starting background geolocation");
    Ti.Geolocation.preferredProvider = "gps";
    Ti.Geolocation.purpose = "GPS demo";
    if (false === Titanium.Geolocation.locationServicesEnabled) Titanium.UI.createAlertDialog({
        title: "Wish-it",
        message: "Your device has geo turned off - turn it on."
    }).show(); else {
        var authorization = Titanium.Geolocation.locationServicesAuthorization;
        Ti.API.info("Authorization: " + authorization);
        authorization == Titanium.Geolocation.AUTHORIZATION_DENIED ? Ti.UI.createAlertDialog({
            title: "Kitchen Sink",
            message: "You have disallowed Titanium from running geolocation services."
        }).show() : authorization == Titanium.Geolocation.AUTHORIZATION_RESTRICTED && Ti.UI.createAlertDialog({
            title: "Kitchen Sink",
            message: "Your system has disallowed Titanium from running geolocation services."
        }).show();
    }
    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    Titanium.Geolocation.distanceFilter = 1;
    var locationCallback = function(e) {
        Ti.API.info("firing background location ");
        if (!e.success || e.error) return;
        var longitude1 = e.coords.longitude;
        var latitude1 = e.coords.latitude;
        e.coords.altitude;
        e.coords.heading;
        var accuracy = e.coords.accuracy;
        e.coords.speed;
        var timestamp = e.coords.timestamp;
        e.coords.altitudeAccuracy;
        Titanium.API.info("geo - location updated: " + new Date(timestamp) + " long " + longitude1 + " lat " + latitude1 + " accuracy " + accuracy);
        Titanium.API.info(e);
    };
    Titanium.Geolocation.addEventListener("location", locationCallback);
}

Ti.API.info("bg-service1: has started");

var listener = Ti.App.currentService.addEventListener("stop", function() {
    Ti.API.info("bg-service1: Although the service has been stopped, it is still registered and will be executed again on next pause");
});