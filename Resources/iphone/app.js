var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

var Module = require("net.iamyellow.tiws");

Alloy.Globals.Map = require("ti.map");

Alloy.Globals.hosturl = "http://10.1.8.77:3000";

var init = require("js/init");

init.startGeolocation();

var mymodels = require("js/wishitModels");

var friendsList = new mymodels.friendsCollection();

friendsList.on("add", function() {
    Ti.API.info("adding stuff");
});

friendsList.on("sync", function() {
    Ti.API.info("adding sync");
});

friendsList.on("remove", function(model) {
    Ti.API.info("remove" + JSON.stringify(model));
    model.destroy();
});

Alloy.Globals.friendsList = friendsList;

Alloy.Collections.friendsList = friendsList;

var friendsPlacesList = new mymodels.friendsPlacesCollection();

friendsPlacesList.on("remove", function(model) {
    Ti.API.info("remove" + JSON.stringify(model));
    model.destroy({
        id: "sds"
    });
});

friendsPlacesList.on("reset", function(model) {
    Ti.API.info("reset" + JSON.stringify(model));
});

friendsPlacesList.on("change", function(model) {
    Ti.API.info("change" + JSON.stringify(model));
});

friendsPlacesList.on("add", function(model) {
    Ti.API.info("add" + JSON.stringify(model));
});

Alloy.Globals.friendsPlacesList = friendsPlacesList;

Alloy.Collections.friendsPlacesList = friendsPlacesList;

var placesList = new mymodels.placesCollection();

placesList.on("remove", function(model) {
    Ti.API.info("remove" + JSON.stringify(model));
    model.destroy({
        id: "sds"
    });
});

placesList.on("reset", function(model) {
    Ti.API.info("reset" + JSON.stringify(model));
});

placesList.on("change", function(model) {
    Ti.API.info("change" + JSON.stringify(model));
});

placesList.on("add", function(model) {
    Ti.API.info("add" + JSON.stringify(model));
});

Alloy.Globals.placesList = placesList;

Alloy.Collections.placesList = placesList;

var requestsList = new mymodels.requestsCollection();

requestsList.on("remove", function(model) {
    Ti.API.info("remove" + JSON.stringify(model));
});

requestsList.on("add", function(model) {
    Ti.API.info("request List add-------------");
    Ti.API.info(model);
});

requestsList.on("change", function(model) {
    Ti.API.info("request List change----------");
    Ti.API.info(model);
});

requestsList.on("reset", function() {
    Ti.API.info("request List reset----------");
});

Alloy.Globals.requestsList = requestsList;

Alloy.Collections.requestsList = requestsList;

Ti.Network.addEventListener("change", function() {
    Ti.API.info("networkType: " + Ti.Network.networkType);
    Ti.API.info("NETWORK_WIFI: " + Ti.Network.NETWORK_WIFI);
    Ti.API.info("NETWORK_UNKNOWN: " + Ti.Network.NETWORK_UNKNOWN);
    Ti.API.info("NETWORK_MOBILE: " + Ti.Network.NETWORK_MOBILE);
    Ti.API.info("NETWORK_LAN: " + Ti.Network.NETWORK_LAN);
    Ti.API.info("NETWORK_NONE: " + Ti.Network.NETWORK_NONE);
});

var service = Ti.App.iOS.registerBackgroundService({
    url: "bg_service.js"
});

Ti.App.addEventListener("pause", function(e) {
    Ti.API.info("the app just pause" + e);
});

Ti.App.addEventListener("paused", function(e) {
    Ti.API.info("the app just paused" + e);
});

Ti.App.addEventListener("resume", function(e) {
    Ti.API.info("the app just resume" + e);
});

Ti.App.addEventListener("resumed", function(e) {
    Ti.API.info("the app just resumed" + e);
});

var websocketMethods = require("js/socketMethods");

Alloy.Globals.WebSocketConnect;

Alloy.Globals.webSocketConnected = false;

Alloy.Globals.startSockets = websocketMethods.websocketStart;

Alloy.Globals.doPost = function(data, url, cookie, loadCallBack, errCallBack) {
    Ti.API.info("checkIfSessionExists start");
    var xhr = Titanium.Network.createHTTPClient();
    xhr.clearCookies(Alloy.Globals.hosturl);
    xhr.onload = loadCallBack;
    xhr.onerror = errCallBack;
    xhr.open("POST", url);
    cookie && xhr.setRequestHeader("Cookie", cookie);
    xhr.send(data);
};

Alloy.createController("index");