function WPATH(s) {
    var index = s.lastIndexOf("/");
    var path = -1 === index ? "ds.slideMenu/" + s : s.substring(0, index) + "/ds.slideMenu/" + s.substring(index + 1);
    return path;
}

module.exports = [ {
    isId: true,
    priority: 100000.0002,
    key: "leftMenu",
    style: {
        top: "0dp",
        left: "0dp",
        width: "250dp",
        zIndex: "2",
        backgroundColor: "#FFF"
    }
}, {
    isId: true,
    priority: 100000.0003,
    key: "rightMenu",
    style: {
        top: "0dp",
        right: "0dp",
        width: "250dp",
        zIndex: "1",
        backgroundColor: "#FFF"
    }
}, {
    isId: true,
    priority: 100000.0004,
    key: "navview",
    style: {
        top: "0dp",
        left: "0dp",
        width: Ti.Platform.displayCaps.platformWidth,
        height: "64",
        backgroundColor: "blue"
    }
}, {
    isId: true,
    priority: 100000.0005,
    key: "navviewScroll",
    style: {
        top: "0dp",
        left: "0dp",
        width: Ti.Platform.displayCaps.platformWidth,
        height: "64",
        backgroundColor: "blue",
        visible: false
    }
}, {
    isId: true,
    priority: 100000.0006,
    key: "movableview",
    style: {
        left: "0",
        zIndex: "3",
        width: Ti.Platform.displayCaps.platformWidth
    }
}, {
    isId: true,
    priority: 100000.0007,
    key: "contentview",
    style: {
        left: "0dp",
        width: Ti.Platform.displayCaps.platformWidth,
        height: Ti.UI.Fill,
        top: "64",
        backgroundColor: "white"
    }
}, {
    isId: true,
    priority: 100000.0008,
    key: "shadowview",
    style: {
        shadowColor: "black",
        shadowOffset: {
            x: "0",
            y: "0"
        },
        shadowRadius: "2.5"
    }
}, {
    isId: true,
    priority: 100000.0009,
    key: "leftButton",
    style: {
        backgroundImage: "none",
        image: "/ds.slideMenu/ButtonMenu.png",
        left: "0",
        top: "10",
        width: "60",
        height: "44",
        style: "none"
    }
}, {
    isId: true,
    priority: 100000.001,
    key: "rightButton",
    style: {
        backgroundImage: "none",
        image: "/ds.slideMenu/ButtonMenu.png",
        right: "0",
        top: "10",
        width: "60",
        height: "44",
        style: "none"
    }
}, {
    isId: true,
    priority: 100000.0011,
    key: "backButton",
    style: {
        left: "10",
        top: "10",
        width: "60",
        height: "44",
        style: "none"
    }
}, {
    isId: true,
    priority: 100000.0012,
    key: "clbBtn",
    style: {
        right: "10",
        top: "10",
        width: "60",
        height: "44",
        style: "none"
    }
}, {
    isId: true,
    priority: 100000.0013,
    key: "leftTableView",
    style: {
        backgroundColor: "red"
    }
}, {
    isId: true,
    priority: 100000.0014,
    key: "rightTableView",
    style: {
        backgroundColor: "red"
    }
} ];