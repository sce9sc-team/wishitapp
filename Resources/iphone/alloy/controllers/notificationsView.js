function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "notificationsView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.notificationsView = Ti.UI.createView({
        id: "notificationsView",
        backgroundColor: "red"
    });
    $.__views.notificationsView && $.addTopLevelView($.__views.notificationsView);
    $.__views.wishItImage = Ti.UI.createImageView({
        id: "wishItImage",
        width: "50",
        height: "50",
        image: "images/wishIt_btn1.png"
    });
    $.__views.notificationsView.add($.__views.wishItImage);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;