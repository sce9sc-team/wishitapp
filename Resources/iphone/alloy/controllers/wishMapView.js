function Controller() {
    function scroll() {
        var views = $.wishMapView.views;
        Ti.API.info(views);
        $.wishMapView.scrollToView(1);
        Alloy.Globals.clbBtn = saveEverything;
        Ti.App.fireEvent("changeNav", {
            text: "Add a Wish",
            button: {
                name: "Save"
            }
        });
    }
    function openImageCapture() {
        alert(1);
    }
    function addDescription() {
        var descView = Alloy.createController("wishViewMore/descriptionView").getView();
        $.addWishTable.appendRow(descView);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "wishMapView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    var __alloyId22 = [];
    $.__views.mapView = Ti.UI.createView({
        id: "mapView",
        title: "Wish-it",
        backgroundColor: "#336699",
        layout: "vertical"
    });
    __alloyId22.push($.__views.mapView);
    $.__views.mapSearch = Ti.UI.createSearchBar({
        id: "mapSearch",
        barColor: "blue",
        showCancel: "false",
        height: "43"
    });
    $.__views.mapView.add($.__views.mapSearch);
    var __alloyId23 = [];
    $.__views.sectionFruit = Ti.UI.createTableViewSection({
        id: "sectionFruit",
        headerTitle: "Fruit"
    });
    __alloyId23.push($.__views.sectionFruit);
    $.__views.__alloyId24 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId24"
    });
    $.__views.sectionFruit.add($.__views.__alloyId24);
    $.__views.__alloyId25 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId25"
    });
    $.__views.sectionFruit.add($.__views.__alloyId25);
    $.__views.__alloyId26 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId26"
    });
    $.__views.sectionFruit.add($.__views.__alloyId26);
    $.__views.__alloyId27 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId27"
    });
    $.__views.sectionFruit.add($.__views.__alloyId27);
    $.__views.__alloyId28 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId28"
    });
    $.__views.sectionFruit.add($.__views.__alloyId28);
    $.__views.__alloyId29 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId29"
    });
    $.__views.sectionFruit.add($.__views.__alloyId29);
    $.__views.__alloyId30 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId30"
    });
    $.__views.sectionFruit.add($.__views.__alloyId30);
    $.__views.__alloyId31 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId31"
    });
    $.__views.sectionFruit.add($.__views.__alloyId31);
    $.__views.mapSearchTable = Ti.UI.createTableView({
        data: __alloyId23,
        id: "mapSearchTable",
        height: "0",
        visible: "false"
    });
    $.__views.mapView.add($.__views.mapSearchTable);
    $.__views.wishMap = Alloy.Globals.Map.createView({
        ns: "Alloy.Globals.Map",
        id: "wishMap",
        pitchEnabled: "true",
        userLocationButton: "true",
        userLocation: "true",
        regionFit: "true",
        animate: "true"
    });
    $.__views.mapView.add($.__views.wishMap);
    $.__views.menu = Ti.UI.createView({
        id: "menu",
        layout: "vertical",
        visible: "true",
        width: "100",
        height: "100",
        bottom: "10"
    });
    $.__views.wishMap.add($.__views.menu);
    $.__views.wishItImage = Ti.UI.createImageView({
        id: "wishItImage",
        image: "images/wishIt_btn1.png"
    });
    $.__views.menu.add($.__views.wishItImage);
    scroll ? $.__views.wishItImage.addEventListener("click", scroll) : __defers["$.__views.wishItImage!click!scroll"] = true;
    $.__views.menu1 = Ti.UI.createView({
        id: "menu1",
        layout: "vertical",
        visible: "true",
        width: "50",
        left: "10",
        height: "50",
        top: "10"
    });
    $.__views.wishMap.add($.__views.menu1);
    $.__views.__alloyId32 = Ti.UI.createButton({
        title: "refresh",
        id: "__alloyId32"
    });
    $.__views.menu1.add($.__views.__alloyId32);
    $.__views.addAWishView = Ti.UI.createView({
        id: "addAWishView",
        title: "Add a Wish"
    });
    __alloyId22.push($.__views.addAWishView);
    $.__views.__alloyId33 = Ti.UI.createView({
        layout: "vertical",
        top: "0",
        height: Titanium.UI.SIZE,
        width: Titanium.UI.FILL,
        id: "__alloyId33"
    });
    $.__views.addAWishView.add($.__views.__alloyId33);
    $.__views.__alloyId34 = Ti.UI.createView({
        layout: "horizontal",
        height: Titanium.UI.SIZE,
        id: "__alloyId34"
    });
    $.__views.__alloyId33.add($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createLabel({
        text: "Add",
        id: "__alloyId35"
    });
    $.__views.__alloyId34.add($.__views.__alloyId35);
    $.__views.scrollMenu = Ti.UI.createScrollView({
        id: "scrollMenu",
        height: "50",
        width: "285",
        backgroundColor: "blue"
    });
    $.__views.__alloyId34.add($.__views.scrollMenu);
    $.__views.__alloyId36 = Ti.UI.createView({
        width: "340",
        backgroundColor: "red",
        height: "50",
        layout: "horizontal",
        id: "__alloyId36"
    });
    $.__views.scrollMenu.add($.__views.__alloyId36);
    $.__views.__alloyId37 = Ti.UI.createButton({
        title: "Image",
        height: "50",
        width: "70",
        backgroundColor: "blue",
        borderColor: "white",
        borderWidth: "1",
        id: "__alloyId37"
    });
    $.__views.__alloyId36.add($.__views.__alloyId37);
    openImageCapture ? $.__views.__alloyId37.addEventListener("click", openImageCapture) : __defers["$.__views.__alloyId37!click!openImageCapture"] = true;
    $.__views.__alloyId38 = Ti.UI.createButton({
        title: "Description",
        height: "50",
        width: "100",
        backgroundColor: "blue",
        borderColor: "white",
        borderWidth: "1",
        id: "__alloyId38"
    });
    $.__views.__alloyId36.add($.__views.__alloyId38);
    addDescription ? $.__views.__alloyId38.addEventListener("click", addDescription) : __defers["$.__views.__alloyId38!click!addDescription"] = true;
    $.__views.__alloyId39 = Ti.UI.createButton({
        title: "Category",
        height: "50",
        width: "100",
        backgroundColor: "blue",
        borderColor: "white",
        borderWidth: "1",
        id: "__alloyId39"
    });
    $.__views.__alloyId36.add($.__views.__alloyId39);
    $.__views.__alloyId40 = Ti.UI.createButton({
        title: "Price",
        height: "50",
        width: "70",
        backgroundColor: "blue",
        borderColor: "white",
        borderWidth: "1",
        id: "__alloyId40"
    });
    $.__views.__alloyId36.add($.__views.__alloyId40);
    var __alloyId41 = [];
    $.__views.__alloyId42 = Ti.UI.createTableViewRow({
        id: "__alloyId42"
    });
    __alloyId41.push($.__views.__alloyId42);
    $.__views.__alloyId43 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        width: Titanium.UI.FILL,
        layout: "vertical",
        id: "__alloyId43"
    });
    $.__views.__alloyId42.add($.__views.__alloyId43);
    $.__views.__alloyId44 = Ti.UI.createLabel({
        text: "Place",
        left: "10",
        color: "blue",
        id: "__alloyId44"
    });
    $.__views.__alloyId43.add($.__views.__alloyId44);
    $.__views.__alloyId45 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        width: Titanium.UI.FILL,
        layout: "horizontal",
        backgroundColor: "orange",
        id: "__alloyId45"
    });
    $.__views.__alloyId43.add($.__views.__alloyId45);
    $.__views.mapAddPlace = Alloy.Globals.Map.createView({
        ns: "Alloy.Globals.Map",
        zoom: "4",
        id: "mapAddPlace",
        touchEnabled: "true",
        height: "100",
        width: "100",
        userLocation: "false"
    });
    $.__views.__alloyId45.add($.__views.mapAddPlace);
    $.__views.__alloyId46 = Ti.UI.createView({
        height: "100",
        layout: "vertical",
        id: "__alloyId46"
    });
    $.__views.__alloyId45.add($.__views.__alloyId46);
    $.__views.__alloyId47 = Ti.UI.createView({
        height: "30",
        layout: "horizontal",
        id: "__alloyId47"
    });
    $.__views.__alloyId46.add($.__views.__alloyId47);
    $.__views.place_name = Ti.UI.createTextField({
        id: "place_name",
        hintText: "Name of Place",
        clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,
        top: "0",
        left: "0",
        width: Titanium.UI.FILL,
        height: "30",
        backgroundColor: "white"
    });
    $.__views.__alloyId47.add($.__views.place_name);
    $.__views.__alloyId48 = Ti.UI.createView({
        height: "40",
        layout: "horizontal",
        id: "__alloyId48"
    });
    $.__views.__alloyId46.add($.__views.__alloyId48);
    $.__views.address_text = Ti.UI.createTextField({
        id: "address_text",
        clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,
        top: "5",
        left: "0",
        width: Titanium.UI.FILL,
        height: "30",
        backgroundColor: "white"
    });
    $.__views.__alloyId48.add($.__views.address_text);
    $.__views.__alloyId49 = Ti.UI.createView({
        height: "30",
        layout: "horizontal",
        id: "__alloyId49"
    });
    $.__views.__alloyId46.add($.__views.__alloyId49);
    $.__views.__alloyId50 = Ti.UI.createButton({
        title: "Bar",
        color: "#007FFF",
        height: "30",
        backgroundColor: "white",
        id: "__alloyId50"
    });
    $.__views.__alloyId49.add($.__views.__alloyId50);
    $.__views.__alloyId51 = Ti.UI.createButton({
        title: "Add",
        color: "#007FFF",
        height: "30",
        backgroundColor: "white",
        id: "__alloyId51"
    });
    $.__views.__alloyId49.add($.__views.__alloyId51);
    $.__views.__alloyId52 = Ti.UI.createTableViewRow({
        id: "__alloyId52"
    });
    __alloyId41.push($.__views.__alloyId52);
    $.__views.__alloyId53 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        width: Titanium.UI.FILL,
        layout: "vertical",
        id: "__alloyId53"
    });
    $.__views.__alloyId52.add($.__views.__alloyId53);
    $.__views.__alloyId54 = Ti.UI.createLabel({
        text: "Your Wish",
        left: "10",
        color: "blue",
        id: "__alloyId54"
    });
    $.__views.__alloyId53.add($.__views.__alloyId54);
    $.__views.__alloyId55 = Ti.UI.createView({
        height: Titanium.UI.SIZE,
        width: Titanium.UI.FILL,
        backgroundColor: "orange",
        layout: "horizontal",
        id: "__alloyId55"
    });
    $.__views.__alloyId53.add($.__views.__alloyId55);
    $.__views.p_name = Ti.UI.createTextField({
        hintText: "Type your Wish",
        clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,
        id: "p_name",
        top: "0",
        width: Titanium.UI.FILL,
        height: "40",
        backgroundColor: "white"
    });
    $.__views.__alloyId55.add($.__views.p_name);
    $.__views.__alloyId56 = Ti.UI.createTableViewRow({
        id: "__alloyId56"
    });
    __alloyId41.push($.__views.__alloyId56);
    $.__views.__alloyId57 = Ti.UI.createView({
        height: "60",
        width: Titanium.UI.FILL,
        layout: "vertical",
        id: "__alloyId57"
    });
    $.__views.__alloyId56.add($.__views.__alloyId57);
    $.__views.__alloyId58 = Ti.UI.createLabel({
        text: "Sharing",
        left: "10",
        color: "blue",
        id: "__alloyId58"
    });
    $.__views.__alloyId57.add($.__views.__alloyId58);
    var __alloyId60 = [];
    var __alloyId64 = {
        title: "Private",
        ns: "Alloy.Abstract"
    };
    __alloyId60.push(__alloyId64);
    var __alloyId65 = {
        title: "Public",
        ns: "Alloy.Abstract"
    };
    __alloyId60.push(__alloyId65);
    var __alloyId66 = {
        title: "Restricted",
        ns: "Alloy.Abstract"
    };
    __alloyId60.push(__alloyId66);
    $.__views.bb1 = Ti.UI.iOS.createTabbedBar({
        labels: __alloyId60,
        borderRadius: "1",
        id: "bb1",
        backgroundColor: "blue",
        height: "25"
    });
    $.__views.__alloyId57.add($.__views.bb1);
    $.__views.addWishTable = Ti.UI.createTableView({
        data: __alloyId41,
        id: "addWishTable",
        allowsSelection: "false"
    });
    $.__views.__alloyId33.add($.__views.addWishTable);
    $.__views.wishMapView = Ti.UI.createScrollableView({
        views: __alloyId22,
        id: "wishMapView",
        currentPage: "0",
        scrollingEnabled: "false",
        disableBounce: "true"
    });
    $.__views.wishMapView && $.addTopLevelView($.__views.wishMapView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("sliderToggled", function(e) {
        Ti.API.info(e);
        $.wishMapView.touchEnabled = e.hasSlided ? false : true;
    });
    var saveEverything = function() {
        alert("Saving Everything");
        var views = $.wishMapView.views;
        Ti.API.info(views);
        alert(views);
    };
    $.wishMapView.addEventListener("scrollend", function() {});
    $.mapSearch.addEventListener("focus", function() {
        $.mapSearchTable.setHeight(150);
        $.mapSearchTable.setVisible(true);
        $.mapSearch.setShowCancel(true, {
            animated: true
        });
    });
    $.mapSearch.addEventListener("blur", function() {
        $.mapSearchTable.setHeight(0);
        $.mapSearchTable.setVisible(false);
        $.mapSearch.setShowCancel(false, {
            animated: true
        });
    });
    $.mapSearch.addEventListener("cancel", function() {
        $.mapSearch.blur();
    });
    $.mapSearch.addEventListener("return", function() {});
    __defers["$.__views.wishItImage!click!scroll"] && $.__views.wishItImage.addEventListener("click", scroll);
    __defers["$.__views.__alloyId37!click!openImageCapture"] && $.__views.__alloyId37.addEventListener("click", openImageCapture);
    __defers["$.__views.__alloyId38!click!addDescription"] && $.__views.__alloyId38.addEventListener("click", addDescription);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;