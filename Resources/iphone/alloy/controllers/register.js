function Controller() {
    function authOnLoad(e) {
        Ti.API.info("in utf-8 onload for POST");
        var rawcookie = this.getResponseHeader("Set-Cookie");
        var data = JSON.parse(this.responseData);
        if (data.res) {
            var user = {
                cookie: rawcookie.split(" "),
                authenticated: true,
                user_id: data.res.user_id,
                username: data.res.username,
                password: data.res.password
            };
            Ti.App.Properties.setObject("user", user);
            Ti.API.info("user saved and authenticated");
            $.trigger("openApp", e);
            Alloy.Globals.startSockets(user);
        } else data.error && Ti.API.log(data.error);
    }
    function authOnError(e) {
        Ti.API.info(e);
    }
    function signinUser() {
        var data = {
            username: $.s_username.getValue(),
            password: $.s_password.getValue()
        };
        Alloy.Globals.doPost(data, Alloy.Globals.hosturl + "/login", null, authOnLoad, authOnError);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "register";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.register = Ti.UI.createView({
        backgroundColor: "green",
        id: "register"
    });
    $.__views.register && $.addTopLevelView($.__views.register);
    $.__views.signinView = Ti.UI.createScrollView({
        width: "auto",
        height: "auto",
        id: "signinView",
        visible: "true",
        backgroundColor: "green",
        layout: "vertical"
    });
    $.__views.register.add($.__views.signinView);
    $.__views.s_username = Ti.UI.createTextField({
        id: "s_username",
        top: "130",
        width: "200",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED
    });
    $.__views.signinView.add($.__views.s_username);
    $.__views.s_password = Ti.UI.createTextField({
        id: "s_password",
        top: "20",
        width: "200",
        borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED
    });
    $.__views.signinView.add($.__views.s_password);
    $.__views.header = Ti.UI.createLabel({
        text: "Sign In",
        id: "header",
        top: "10"
    });
    $.__views.signinView.add($.__views.header);
    signinUser ? $.__views.header.addEventListener("click", signinUser) : __defers["$.__views.header!click!signinUser"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.header!click!signinUser"] && $.__views.header.addEventListener("click", signinUser);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;