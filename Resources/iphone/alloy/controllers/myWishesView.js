function Controller() {
    function __alloyId21(e) {
        if (e && e.fromAdapter) return;
        __alloyId21.opts || {};
        var models = __alloyId20.models;
        var len = models.length;
        var rows = [];
        for (var i = 0; len > i; i++) {
            var __alloyId12 = models[i];
            __alloyId12.__transform = {};
            var __alloyId14 = Ti.UI.createTableViewRow({
                height: "100",
                place_id: "undefined" != typeof __alloyId12.__transform["id"] ? __alloyId12.__transform["id"] : __alloyId12.get("id"),
                searchText: "undefined" != typeof __alloyId12.__transform["searchText"] ? __alloyId12.__transform["searchText"] : __alloyId12.get("searchText")
            });
            rows.push(__alloyId14);
            var __alloyId16 = Ti.UI.createImageView({
                image: "undefined" != typeof __alloyId12.__transform["image_url"] ? __alloyId12.__transform["image_url"] : __alloyId12.get("image_url"),
                width: "90",
                height: "90",
                left: "5",
                top: "5"
            });
            __alloyId14.add(__alloyId16);
            var __alloyId17 = Ti.UI.createLabel({
                text: "undefined" != typeof __alloyId12.__transform["name"] ? __alloyId12.__transform["name"] : __alloyId12.get("name"),
                color: "black",
                top: "10",
                left: "110"
            });
            __alloyId14.add(__alloyId17);
            var __alloyId18 = Ti.UI.createLabel({
                text: "undefined" != typeof __alloyId12.__transform["desc_item"] ? __alloyId12.__transform["desc_item"] : __alloyId12.get("desc_item"),
                color: "gray",
                left: "110",
                top: "40"
            });
            __alloyId14.add(__alloyId18);
            var __alloyId19 = Ti.UI.createLabel({
                text: "undefined" != typeof __alloyId12.__transform["distance"] ? __alloyId12.__transform["distance"] : __alloyId12.get("distance"),
                color: "gray",
                left: "110",
                top: "75"
            });
            __alloyId14.add(__alloyId19);
        }
        $.__views.myPlacesTable.setData(rows);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "myWishesView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.myWishesView = Ti.UI.createView({
        id: "myWishesView"
    });
    $.__views.myWishesView && $.addTopLevelView($.__views.myWishesView);
    $.__views.search = Ti.UI.createSearchBar({
        id: "search",
        barColor: "#000",
        showCancel: "false",
        height: "43",
        top: "0"
    });
    $.__views.myPlacesTable = Ti.UI.createTableView({
        search: $.__views.search,
        id: "myPlacesTable",
        editable: "true",
        filterAttribute: "searchText"
    });
    $.__views.myWishesView.add($.__views.myPlacesTable);
    var __alloyId20 = Alloy.Collections["placesList"] || placesList;
    __alloyId20.on("fetch destroy change add remove reset", __alloyId21);
    exports.destroy = function() {
        __alloyId20.off("fetch destroy change add remove reset", __alloyId21);
    };
    _.extend($, $.__views);
    Alloy.Globals.placesList.fetch();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;