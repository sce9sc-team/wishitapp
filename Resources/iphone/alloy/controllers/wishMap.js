function Controller() {
    function scroll() {
        var views = $.wishMap.views;
        Ti.API.info(views);
        $.wishMap.scrollToView(1);
        Ti.App.fireEvent("changeNav", {
            text: "dasdad"
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "wishMap";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    var __alloyId12 = [];
    $.__views.view1_1 = Ti.UI.createView({
        id: "view1_1",
        backgroundColor: "#336699"
    });
    __alloyId12.push($.__views.view1_1);
    $.__views.wishMap = Alloy.Globals.Map.createView({
        ns: "Alloy.Globals.Map",
        id: "wishMap",
        pitchEnabled: "true",
        userLocationButton: "true",
        userLocation: "true",
        regionFit: "true",
        animate: "true"
    });
    $.__views.view1_1.add($.__views.wishMap);
    $.__views.menu = Ti.UI.createView({
        id: "menu",
        layout: "vertical",
        visible: "true",
        width: "100",
        height: "100",
        bottom: "10"
    });
    $.__views.wishMap.add($.__views.menu);
    $.__views.image = Ti.UI.createImageView({
        id: "image",
        image: "images/wishit_btn1.png"
    });
    $.__views.menu.add($.__views.image);
    scroll ? $.__views.image.addEventListener("click", scroll) : __defers["$.__views.image!click!scroll"] = true;
    $.__views.menu1 = Ti.UI.createView({
        id: "menu1",
        layout: "vertical",
        visible: "true",
        width: "50",
        left: "10",
        height: "50",
        top: "10"
    });
    $.__views.wishMap.add($.__views.menu1);
    $.__views.__alloyId13 = Ti.UI.createButton({
        title: "refresh",
        id: "__alloyId13"
    });
    $.__views.menu1.add($.__views.__alloyId13);
    $.__views.view1_4 = Ti.UI.createView({
        id: "view1_4",
        backgroundColor: "red"
    });
    __alloyId12.push($.__views.view1_4);
    var __alloyId14 = [];
    $.__views.sectionFruit = Ti.UI.createTableViewSection({
        id: "sectionFruit",
        headerTitle: "Fruit"
    });
    __alloyId14.push($.__views.sectionFruit);
    $.__views.__alloyId15 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId15"
    });
    $.__views.sectionFruit.add($.__views.__alloyId15);
    $.__views.__alloyId16 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId16"
    });
    $.__views.sectionFruit.add($.__views.__alloyId16);
    $.__views.sectionVeg = Ti.UI.createTableViewSection({
        id: "sectionVeg",
        headerTitle: "Vegetables"
    });
    __alloyId14.push($.__views.sectionVeg);
    $.__views.__alloyId17 = Ti.UI.createTableViewRow({
        title: "Carrots",
        id: "__alloyId17"
    });
    $.__views.sectionVeg.add($.__views.__alloyId17);
    $.__views.__alloyId18 = Ti.UI.createTableViewRow({
        title: "Potatoes",
        id: "__alloyId18"
    });
    $.__views.sectionVeg.add($.__views.__alloyId18);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId14.push($.__views.sectionFish);
    $.__views.__alloyId19 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId19"
    });
    $.__views.sectionFish.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId20"
    });
    $.__views.sectionFish.add($.__views.__alloyId20);
    $.__views.table = Ti.UI.createTableView({
        data: __alloyId14,
        id: "table"
    });
    $.__views.view1_4.add($.__views.table);
    $.__views.wishMap = Ti.UI.createScrollableView({
        views: __alloyId12,
        id: "wishMap",
        currentPage: "0",
        scrollingEnabled: "false",
        disableBounce: "true"
    });
    $.__views.wishMap && $.addTopLevelView($.__views.wishMap);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("sliderToggled", function(e) {
        Ti.API.info(e);
        e.hasSlided;
    });
    $.wishMap.addEventListener("scrollend", function(e) {
        alert("on scrollend");
        alert(e);
    });
    __defers["$.__views.image!click!scroll"] && $.__views.image.addEventListener("click", scroll);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;