function Controller() {
    function onloadCallBck() {
        Ti.API.info("checkIfSessionExists res");
        var rawcookie = this.getResponseHeader("Set-Cookie");
        var data = JSON.parse(this.responseData);
        if (data.res) {
            var user = Ti.App.Properties.getObject("user");
            if (rawcookie) {
                user.cookie = rawcookie.split(" ");
                Ti.App.Properties.setObject("user", user);
                Ti.API.info("user updated");
            }
            Alloy.Globals.authorizeUser = true;
            Alloy.Globals.trytoLoginInterval = false;
            Alloy.Globals.startSockets(user);
        } else data.error && Ti.API.log(data.error);
    }
    function onErrorCallBck(e) {
        Ti.API.info(e);
        if (!Alloy.Globals.trytoLoginInterval) {
            Alloy.Globals.trytoLoginInterval = true;
            Alloy.Globals.trytoLoginCall = setInterval(function() {
                if (Alloy.Globals.authorizeUser) {
                    clearInterval(Alloy.Globals.trytoLoginCall);
                    Alloy.Globals.trytoLoginInterval = false;
                } else trytologin();
            }, 5e3);
        }
    }
    function authorizeUser(user) {
        $.winScroll.setCurrentPage(1);
        var data = {
            username: user.username,
            password: user.password
        };
        Alloy.Globals.doPost(data, Alloy.Globals.hosturl + "/login", user.cookie[0], onloadCallBck, onErrorCallBck);
    }
    function trytologin() {
        if (Ti.App.Properties.hasProperty("user")) {
            Ti.API.info("Checking if user is authorized");
            var user = Ti.App.Properties.getObject("user");
            Ti.API.info(user);
            user.authenticated && authorizeUser(user);
        } else Ti.API.info("user does not exist");
    }
    function rowSelect(e) {
        var views = $.ds.contentview.getViews();
        var currentView = views[$.ds.contentview.getCurrentPage()];
        if (currentView.id != e.row.viewsrc) {
            var num = _.map(views, function(num) {
                return num.id;
            }).indexOf(e.row.viewsrc);
            if (-1 == num) {
                Ti.API.info(JSON.stringify(views));
                var newView = Alloy.createController(e.row.viewsrc).getView();
                $.ds.contentview.addView(newView);
                $.ds.contentview.setCurrentPage(views.length);
            } else $.ds.contentview.setCurrentPage(num);
            $.ds.navTitle.setText(e.row.title);
        }
    }
    function setupUI() {
        var leftData = [];
        var rightData = [];
        var leftMenuData = [ {
            name: "Wish Map",
            viewsrc: "wishMapView",
            image: null,
            props: null
        }, {
            name: "My Wishes",
            viewsrc: "myWishesView",
            image: null,
            props: null
        }, {
            name: "Friends",
            viewsrc: "myFriendsView",
            image: null,
            props: null
        }, {
            name: "All wishes",
            viewsrc: "myWishesView",
            image: null,
            props: null
        }, {
            name: "Interested",
            viewsrc: "myWishesView",
            image: null,
            props: null
        }, {
            name: "",
            viewsrc: null,
            image: null,
            props: null
        }, {
            name: "Requests",
            viewsrc: "requestsView",
            image: null,
            props: null
        }, {
            name: "Notifications",
            viewsrc: "notificationsView",
            image: null,
            props: null
        }, {
            name: "Settings",
            viewsrc: "notificationsView",
            image: null,
            props: null
        } ];
        var rightMenuData = [ {
            name: "Wish Map",
            viewsrc: "wishMapView",
            image: null,
            props: null
        }, {
            name: "My Wishes",
            viewsrc: "myWishesView",
            image: null,
            props: null
        }, {
            name: "Friends",
            viewsrc: "myFriendsView",
            image: null,
            props: null
        }, {
            name: "All wishes",
            viewsrc: "myWishesView",
            image: null,
            props: null
        }, {
            name: "Interested",
            viewsrc: "myWishesView",
            image: null,
            props: null
        }, {
            name: "",
            viewsrc: null,
            image: null,
            props: null
        }, {
            name: "Requests",
            viewsrc: "requestsView",
            image: null,
            props: null
        }, {
            name: "Notifications",
            viewsrc: "notificationsView",
            image: null,
            props: null
        }, {
            name: "Settings",
            viewsrc: "notificationsView",
            image: null,
            props: null
        } ];
        for (var i = 0; leftMenuData.length > i; i++) {
            var rowl = Alloy.createController("leftMenuRow", leftMenuData[i]).getView();
            leftData.push(rowl);
        }
        for (var i = 0; rightMenuData.length > i; i++) {
            var rowr = Alloy.createController("rightMenuRow", rightMenuData[i]).getView();
            rightData.push(rowr);
        }
        $.ds.leftTableView.data = leftData;
        $.ds.rightTableView.data = rightData;
        var firstView = Alloy.createController("wishMapView").getView();
        $.ds.contentview.addView(firstView);
        $.ds.contentview.setCurrentPage(0);
        $.ds.navTitle.setText("Wish-it");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.win = Ti.UI.createWindow({
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    var __alloyId3 = [];
    $.__views.registerWin = Alloy.createController("register", {
        id: "registerWin"
    });
    __alloyId3.push($.__views.registerWin.getViewEx({
        recurse: true
    }));
    $.__views.ds = Alloy.createWidget("ds.slideMenu", "widget", {
        id: "ds"
    });
    __alloyId3.push($.__views.ds.getViewEx({
        recurse: true
    }));
    $.__views.winScroll = Ti.UI.createScrollableView({
        views: __alloyId3,
        id: "winScroll",
        currentPage: "0",
        scrollingEnabled: "false",
        disableBounce: "true"
    });
    $.__views.win.add($.__views.winScroll);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Alloy.Globals.authorizeUser = false;
    Alloy.Globals.trytoLoginCall;
    Alloy.Globals.trytoLoginInterval = false;
    var registered = false;
    if (registered) $.winScroll.setCurrentPage(1); else {
        $.winScroll.setCurrentPage(0);
        trytologin();
    }
    $.registerWin.on("openApp", function() {
        $.winScroll.scrollToView(1);
    });
    setupUI();
    $.ds.leftTableView.addEventListener("click", function(e) {
        if ("" != e.row.viewsrc) {
            rowSelect(e);
            $.ds.toggleLeftSlider();
        }
    });
    $.ds.rightTableView.addEventListener("click", function(e) {
        rowSelect(e);
        $.ds.toggleRightSlider();
    });
    $.ds.leftTableView.addEventListener("touchstart", function() {});
    $.ds.leftTableView.addEventListener("touchend", function() {});
    $.ds.leftTableView.addEventListener("scroll", function() {});
    $.ds.rightTableView.addEventListener("touchstart", function() {});
    $.ds.rightTableView.addEventListener("touchend", function() {});
    $.ds.rightTableView.addEventListener("scroll", function() {});
    Ti.App.addEventListener("sliderToggled", function(e) {
        if ("right" == e.direction) {
            $.ds.leftMenu.zIndex = 2;
            $.ds.rightMenu.zIndex = 1;
        } else if ("left" == e.direction) {
            $.ds.leftMenu.zIndex = 1;
            $.ds.rightMenu.zIndex = 2;
        }
    });
    "iphone" === Ti.Platform.osname ? $.win.open() : $.win.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;