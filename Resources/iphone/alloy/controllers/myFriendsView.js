function Controller() {
    function __alloyId11(e) {
        if (e && e.fromAdapter) return;
        __alloyId11.opts || {};
        var models = __alloyId10.models;
        var len = models.length;
        var rows = [];
        for (var i = 0; len > i; i++) {
            var __alloyId4 = models[i];
            __alloyId4.__transform = {};
            var __alloyId6 = Ti.UI.createTableViewRow({
                height: "100",
                user_id: "undefined" != typeof __alloyId4.__transform["id"] ? __alloyId4.__transform["id"] : __alloyId4.get("id")
            });
            rows.push(__alloyId6);
            var __alloyId8 = Ti.UI.createLabel({
                text: "undefined" != typeof __alloyId4.__transform["username"] ? __alloyId4.__transform["username"] : __alloyId4.get("username")
            });
            __alloyId6.add(__alloyId8);
            var __alloyId9 = Ti.UI.createLabel({
                left: "0",
                text: "undefined" != typeof __alloyId4.__transform["userStatus"] ? __alloyId4.__transform["userStatus"] : __alloyId4.get("userStatus")
            });
            __alloyId6.add(__alloyId9);
        }
        $.__views.myFriendsTable.setData(rows);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "myFriendsView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.myFriendsView = Ti.UI.createView({
        id: "myFriendsView"
    });
    $.__views.myFriendsView && $.addTopLevelView($.__views.myFriendsView);
    $.__views.myFriendsTable = Ti.UI.createTableView({
        id: "myFriendsTable",
        editable: "true"
    });
    $.__views.myFriendsView.add($.__views.myFriendsTable);
    var __alloyId10 = Alloy.Collections["friendsList"] || friendsList;
    __alloyId10.on("fetch destroy change add remove reset", __alloyId11);
    exports.destroy = function() {
        __alloyId10.off("fetch destroy change add remove reset", __alloyId11);
    };
    _.extend($, $.__views);
    Alloy.Globals.friendsList.fetch();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;