function Controller() {
    function scroll() {
        var views = $.view1.views;
        Ti.API.info(views);
        $.view1.scrollToView(1);
        Ti.App.fireEvent("changeNav", {
            text: "dasdad"
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "view1";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    var __alloyId4 = [];
    $.__views.view1_1 = Ti.UI.createView({
        id: "view1_1",
        backgroundColor: "#336699"
    });
    __alloyId4.push($.__views.view1_1);
    $.__views.wishMap = Alloy.Globals.Map.createView({
        ns: "Alloy.Globals.Map",
        id: "wishMap",
        pitchEnabled: "true",
        userLocationButton: "true",
        userLocation: "true",
        regionFit: "true",
        animate: "true"
    });
    $.__views.view1_1.add($.__views.wishMap);
    $.__views.menu = Ti.UI.createView({
        id: "menu",
        layout: "vertical",
        visible: "true",
        width: "100",
        height: "100",
        bottom: "10"
    });
    $.__views.wishMap.add($.__views.menu);
    $.__views.image = Ti.UI.createImageView({
        id: "image",
        image: "images/wishit_btn1.png"
    });
    $.__views.menu.add($.__views.image);
    scroll ? $.__views.image.addEventListener("click", scroll) : __defers["$.__views.image!click!scroll"] = true;
    $.__views.menu1 = Ti.UI.createView({
        id: "menu1",
        layout: "vertical",
        visible: "true",
        width: "50",
        left: "10",
        height: "50",
        top: "10"
    });
    $.__views.wishMap.add($.__views.menu1);
    $.__views.__alloyId5 = Ti.UI.createButton({
        title: "refresh",
        id: "__alloyId5"
    });
    $.__views.menu1.add($.__views.__alloyId5);
    $.__views.view1_4 = Ti.UI.createView({
        id: "view1_4",
        backgroundColor: "red"
    });
    __alloyId4.push($.__views.view1_4);
    var __alloyId6 = [];
    $.__views.sectionFruit = Ti.UI.createTableViewSection({
        id: "sectionFruit",
        headerTitle: "Fruit"
    });
    __alloyId6.push($.__views.sectionFruit);
    $.__views.__alloyId7 = Ti.UI.createTableViewRow({
        title: "Apple",
        id: "__alloyId7"
    });
    $.__views.sectionFruit.add($.__views.__alloyId7);
    $.__views.__alloyId8 = Ti.UI.createTableViewRow({
        title: "Bananas",
        id: "__alloyId8"
    });
    $.__views.sectionFruit.add($.__views.__alloyId8);
    $.__views.sectionVeg = Ti.UI.createTableViewSection({
        id: "sectionVeg",
        headerTitle: "Vegetables"
    });
    __alloyId6.push($.__views.sectionVeg);
    $.__views.__alloyId9 = Ti.UI.createTableViewRow({
        title: "Carrots",
        id: "__alloyId9"
    });
    $.__views.sectionVeg.add($.__views.__alloyId9);
    $.__views.__alloyId10 = Ti.UI.createTableViewRow({
        title: "Potatoes",
        id: "__alloyId10"
    });
    $.__views.sectionVeg.add($.__views.__alloyId10);
    $.__views.sectionFish = Ti.UI.createTableViewSection({
        id: "sectionFish",
        headerTitle: "Fish"
    });
    __alloyId6.push($.__views.sectionFish);
    $.__views.__alloyId11 = Ti.UI.createTableViewRow({
        title: "Cod",
        id: "__alloyId11"
    });
    $.__views.sectionFish.add($.__views.__alloyId11);
    $.__views.__alloyId12 = Ti.UI.createTableViewRow({
        title: "Haddock",
        id: "__alloyId12"
    });
    $.__views.sectionFish.add($.__views.__alloyId12);
    $.__views.table = Ti.UI.createTableView({
        data: __alloyId6,
        id: "table"
    });
    $.__views.view1_4.add($.__views.table);
    $.__views.view1 = Ti.UI.createScrollableView({
        views: __alloyId4,
        id: "view1",
        currentPage: "0",
        scrollingEnabled: "false",
        disableBounce: "true"
    });
    $.__views.view1 && $.addTopLevelView($.__views.view1);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.App.addEventListener("sliderToggled", function(e) {
        Ti.API.info(e);
        $.view1.touchEnabled = e.hasSlided ? false : true;
    });
    $.view1.addEventListener("scrollend", function(e) {
        alert("on scrollend");
        alert(e);
    });
    __defers["$.__views.image!click!scroll"] && $.__views.image.addEventListener("click", scroll);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;